export let PARTS =
	[{
		partName: "الجزء الاول ",
		sour: [
			{
				souraName: "الفاتحه",
				startPage: 1,
				type: 0,
				numberOfPhrases: "7"
			}, {
				souraName: "البقرة",
				startPage: 2,
				type: 1,
				numberOfPhrases: "286"
			}
		]
	}, {
		partName: "الجزء الثانى ",
		sour: [
			{
				souraName: "البقرة",
				startPage: 22,
				type: 1,
				numberOfPhrases: "286"
			}
		]
	}, {
		partName: "الجزء الثالث ",
		sour: [
			{
				souraName: "البقرة",
				startPage: 42,
				type: 1,
				numberOfPhrases: "286"
			},
			{
				souraName: "ال عمران",
				startPage: 50,
				type: 1,
				numberOfPhrases: "176"
			}
		]
	}, {
		partName: "الجزء الرابع ",
		sour: [
			{
				souraName: "ال عمران",
				startPage: 62,
				type: 1,
				numberOfPhrases: "176"
			},
			{
				souraName: "النساء",
				startPage: 76,
				type: 1,
				numberOfPhrases: "176"
			}
		]
	}, {
		partName: "الجزء الخامس ",
		sour: [
			{
				souraName: "النساء",
				startPage: 82,
				type: 1,
				numberOfPhrases: "176"
			}
		]
	}, {
		partName: "الجزء السادس ",
		sour: [
			{
				souraName: "النساء",
				startPage: 102,
				type: 1,
				numberOfPhrases: "176"
			},
			{
				souraName: "المائدة",
				startPage: 106,
				type: 1,
				numberOfPhrases: "120"
			}
		]
	}, {
		partName: "الجزء السابع ",
		sour: [
			{
				souraName: "المائدة",
				startPage: 122,
				type: 1,
				numberOfPhrases: "120"
			},
			{
				souraName: "الانعام",
				startPage: 128,
				type: 0,
				numberOfPhrases: "165"
			}
		]
	}, {
		partName: "الجزء الثامن ",
		sour: [
			{
				souraName: "الانعام",
				startPage: 142,
				type: 0,
				numberOfPhrases: "165"
			},
			{
				souraName: "الاعراف",
				startPage: 151,
				type: 0,
				numberOfPhrases: "206"
			}
		]
	}, {
		partName: "الجزء التاسع ",
		sour: [
			{
				souraName: "الاعراف",
				startPage: 162,
				type: 0,
				numberOfPhrases: "206"
			},
			{
				souraName: "الانفال",
				startPage: 177,
				type: 1,
				numberOfPhrases: "75"
			}
		]
	}, {
		partName: "الجزء العاشر ",
		sour: [
			{
				souraName: "الانفال",
				startPage: 182,
				type: 1,
				numberOfPhrases: "75"
			},
			{
				souraName: "التوبة",
				startPage: 187,
				type: 1,
				numberOfPhrases: "129"
			}
		]
	}, {
		partName: "الجزء الحادي عشر ",
		sour: [
			{
				souraName: "التوبة",
				startPage: 202,
				type: 1,
				numberOfPhrases: "129"
			},
			{
				souraName: "يونس",
				startPage: 207,
				type: 0,
				numberOfPhrases: "109"
			},
			{
				souraName: "هود",
				startPage: 221,
				type: 0,
				numberOfPhrases: "123"
			}
		]
	}, {
		partName: "الجزء الثانى عشر ",
		sour: [
			{
				souraName: "هود",
				startPage: 222,
				type: 0,
				numberOfPhrases: "123"
			},
			{
				souraName: "يوسف",
				startPage: 235,
				type: 0,
				numberOfPhrases: "111"
			}
		]
	}, {
		partName: "الجزء الثالث عشر ",
		sour: [
			{
				souraName: "يوسف",
				startPage: 242,
				type: 0,
				numberOfPhrases: "111"
			},
			{
				souraName: "الرعد",
				startPage: 249,
				type: 1,
				numberOfPhrases: "43"
			},
			{
				souraName: "إبراهيم",
				startPage: 255,
				type: 0,
				numberOfPhrases: "52"
			}
		]
	}, {
		partName: "الجزء الرابع عشر ",
		sour: [
			{
				souraName: "الحجر",
				startPage: 262,
				type: 0,
				numberOfPhrases: "99"
			},
			{
				souraName: "النحل",
				startPage: 267,
				type: 0,
				numberOfPhrases: "128"
			}
		]
	}, {
		partName: "الجزء الخامس عشر ",
		sour: [
			{
				souraName: "الإسراء",
				startPage: 282,
				type: 0,
				numberOfPhrases: "111"
			},
			{
				souraName: "الكهف",
				startPage: 293,
				type: 0,
				numberOfPhrases: "110"
			}
		]
	}, {
		partName: "الجزء السادس عشر ",
		sour: [
			{
				souraName: "الكهف",
				startPage: 302,
				type: 0,
				numberOfPhrases: "110"
			},
			{
				souraName: "مريم",
				startPage: 305,
				type: 0,
				numberOfPhrases: "98"
			},
			{
				souraName: "طه",
				startPage: 312,
				type: 0,
				numberOfPhrases: "135"
			}
		]
	}, {
		partName: "الجزء السابع عشر ",
		sour: [
			{
				souraName: "الأنبياء",
				startPage: 322,
				type: 0,
				numberOfPhrases: "112"
			},
			{
				souraName: "الحج",
				startPage: 331,
				type: 1,
				numberOfPhrases: "78"
			},
			{
				souraName: "المؤمنون",
				startPage: 341,
				type: 0,
				numberOfPhrases: "118"
			}
		]
	}, {
		partName: "الجزء الثامن عشر ",
		sour: [
			{
				souraName: "المؤمنون",
				startPage: 342,
				type: 0,
				numberOfPhrases: "118"
			},
			{
				souraName: "النور",
				startPage: 349,
				type: 1,
				numberOfPhrases: "64"
			},
			{
				souraName: "الفرقان",
				startPage: 359,
				type: 0,
				numberOfPhrases: "77"
			}
		]
	}, {
		partName: "الجزء التاسع عشر ",
		sour: [
			{
				souraName: "الفرقان",
				startPage: 362,
				type: 0,
				numberOfPhrases: "77"
			},
			{
				souraName: "الشعراء",
				startPage: 366,
				type: 0,
				numberOfPhrases: "227"
			},
			{
				souraName: "النمل",
				startPage: 376,
				type: 0,
				numberOfPhrases: "93"
			}
		]
	}, {
		partName: "الجزء العشرون ",
		sour: [
			{
				souraName: "النمل",
				startPage: 382,
				type: 0,
				numberOfPhrases: "93"
			},
			{
				souraName: "القصص",
				startPage: 385,
				type: 0,
				numberOfPhrases: "88"
			},
			{
				souraName: "العنكبوت",
				startPage: 396,
				type: 0,
				numberOfPhrases: "69"
			}
		]
	}, {
		partName: "الجزء الحادى والعشرون ",
		sour: [
			{
				souraName: "العنكبوت",
				startPage: 402,
				type: 0,
				numberOfPhrases: "69"
			},
			{
				souraName: "الروم",
				startPage: 404,
				type: 0,
				numberOfPhrases: "60"
			},
			{
				souraName: "لقمان",
				startPage: 411,
				type: 0,
				numberOfPhrases: "34"
			},
			{
				souraName: "السجدة",
				startPage: 414,
				type: 0,
				numberOfPhrases: "30"
			},
			{
				souraName: "الاحزاب",
				startPage: 417,
				type: 1,
				numberOfPhrases: "73"
			}
		]
	}, {
		partName: "الجزء الثانى والعشرون ",
		sour: [
			{
				souraName: "الاحزاب",
				startPage: 422,
				type: 1,
				numberOfPhrases: "73"
			},
			{
				souraName: "سبأ",
				startPage: 428,
				type: 0,
				numberOfPhrases: "54"
			},
			{
				souraName: "فاطر",
				startPage: 434,
				type: 0,
				numberOfPhrases: "45"
			},
			{
				souraName: "يس",
				startPage: 440,
				type: 0,
				numberOfPhrases: "83"
			}
		]
	}, {
		partName: "الجزء الثالث والعشرون  ",
		sour: [
			{
				souraName: "يس",
				startPage: 442,
				type: 0,
				numberOfPhrases: "83"
			},
			{
				souraName: "الصافات",
				startPage: 445,
				type: 0,
				numberOfPhrases: "182"
			},
			{
				souraName: "ص",
				startPage: 452,
				type: 0,
				numberOfPhrases: "88"
			},
			{
				souraName: "الزمر",
				startPage: 458,
				type: 0,
				numberOfPhrases: "75"
			}
		]
	}, {
		partName: "الجزء الرابع والعشرون ",
		sour: [
			{
				souraName: "الزمر",
				startPage: 462,
				type: 0,
				numberOfPhrases: "75"
			},
			{
				souraName: "غافر",
				startPage: 467,
				type: 0,
				numberOfPhrases: "85"
			},
			{
				souraName: "فصلت",
				startPage: 477,
				type: 0,
				numberOfPhrases: "54"
			}
		]
	}, {
		partName: "الجزء الخامس والعشرون ",
		sour: [
			{
				souraName: "فصلت",
				startPage: 482,
				type: 0,
				numberOfPhrases: "54"
			},
			{
				souraName: "الشورى",
				startPage: 483,
				type: 0,
				numberOfPhrases: "53"
			},
			{
				souraName: "الزخرف",
				startPage: 489,
				type: 0,
				numberOfPhrases: "89"
			},
			{
				souraName: "الدخان",
				startPage: 496,
				type: 0,
				numberOfPhrases: "59"
			},
			{
				souraName: "الجاثية",
				startPage: 498,
				type: 0,
				numberOfPhrases: "37"
			}
		]
	}, {
		partName: "الجزء السادس والعشرون ",
		sour: [
			{
				souraName: "الجاثية",
				startPage: 502,
				type: 0,
				numberOfPhrases: "37"
			},
			{
				souraName: "الاحقاف",
				startPage: 502,
				type: 0,
				numberOfPhrases: "35"
			},
			{
				souraName: "محمد",
				startPage: 506,
				type: 1,
				numberOfPhrases: "38"
			},
			{
				souraName: "الفتح",
				startPage: 511,
				type: 1,
				numberOfPhrases: "29"
			},
			{
				souraName: "الحجرات",
				startPage: 515,
				type: 1,
				numberOfPhrases: "18"
			},
			{
				souraName: "ق",
				startPage: 518,
				type: 0,
				numberOfPhrases: "45"
			},
			{
				souraName: "الذاريات",
				startPage: 520,
				type: 0,
				numberOfPhrases: "60"
			}
		]
	}, {
		partName: "الجزء السابع والعشرون ",
		sour: [
			{
				souraName: "الذاريات",
				startPage: 522,
				type: 0,
				numberOfPhrases: "60"
			},
			{
				souraName: "الطور",
				startPage: 523,
				type: 0,
				numberOfPhrases: "49"
			},
			{
				souraName: "النجم",
				startPage: 525,
				type: 0,
				numberOfPhrases: "62"
			},
			{
				souraName: "القمر",
				startPage: 528,
				type: 0,
				numberOfPhrases: "55"
			},
			{
				souraName: "الرحمن",
				startPage: 531,
				type: 1,
				numberOfPhrases: "78"
			},
			{
				souraName: "الواقعة",
				startPage: 534,
				type: 0,
				numberOfPhrases: "96"
			},
			{
				souraName: "الحديد",
				startPage: 537,
				type: 1,
				numberOfPhrases: "29"
			}
		]
	}, {
		partName: "الجزء الثامن والعشرون ",
		sour: [
			{
				souraName: "المجادلة",
				startPage: 542,
				type: 1,
				numberOfPhrases: "22"
			},
			{
				souraName: "الحشر",
				startPage: 545,
				type: 1,
				numberOfPhrases: "24"
			},
			{
				souraName: "الممتحنة",
				startPage: 548,
				type: 1,
				numberOfPhrases: "13"
			},
			{
				souraName: "الصف",
				startPage: 551,
				type: 1,
				numberOfPhrases: "14"
			},
			{
				souraName: "الجمعة",
				startPage: 553,
				type: 1,
				numberOfPhrases: "11"
			},
			{
				souraName: "المنافقون",
				startPage: 554,
				type: 1,
				numberOfPhrases: "11"
			},
			{
				souraName: "التغابن",
				startPage: 555,
				type: 1,
				numberOfPhrases: "18"
			},
			{
				souraName: "الطلاق",
				startPage: 557,
				type: 1,
				numberOfPhrases: "12"
			},
			{
				souraName: "التحريم",
				startPage: 560,
				type: 1,
				numberOfPhrases: "30"
			}
		]
	}, {
		partName: "الجزء التاسع والعشرون ",
		sour: [
			{
				souraName: "الملك",
				startPage: 562,
				type: 0,
				numberOfPhrases: "30"
			},
			{
				souraName: "القلم",
				startPage: 564,
				type: 0,
				numberOfPhrases: "52"
			},
			{
				souraName: "الحاقة",
				startPage: 566,
				type: 0,
				numberOfPhrases: "52"
			},
			{
				souraName: "المعارج",
				startPage: 568,
				type: 0,
				numberOfPhrases: "44"
			},
			{
				souraName: "نوح",
				startPage: 570,
				type: 0,
				numberOfPhrases: "28"
			},
			{
				souraName: "الجن",
				startPage: 572,
				type: 0,
				numberOfPhrases: "28"
			},
			{
				souraName: "المزمل",
				startPage: 574,
				type: 0,
				numberOfPhrases: "20"
			},
			{
				souraName: "المدثر",
				startPage: 575,
				type: 0,
				numberOfPhrases: "56"
			},
			{
				souraName: "القيامة",
				startPage: 577,
				type: 0,
				numberOfPhrases: "40"
			},
			{
				souraName: "الانسان",
				startPage: 578,
				type: 1,
				numberOfPhrases: "31"
			},
			{
				souraName: "المرسلات",
				startPage: 580,
				type: 0,
				numberOfPhrases: "50"
			}
		]
	}, {
		partName: "الجزء الثلاثون ",
		sour: [
			{
				souraName: "النبأ",
				startPage: 582,
				type: 0,
				numberOfPhrases: "40"
			},
			{
				souraName: "النازعات",
				startPage: 583,
				type: 0,
				numberOfPhrases: "46"
			},
			{
				souraName: "عبس",
				startPage: 584,
				type: 0,
				numberOfPhrases: "42"
			},
			{
				souraName: "التكوير",
				startPage: 586,
				type: 0,
				numberOfPhrases: "29"
			},
			{
				souraName: "الانفطار",
				startPage: 587,
				type: 0,
				numberOfPhrases: "19"
			},
			{
				souraName: "المطففين",
				startPage: 587,
				type: 0,
				numberOfPhrases: "36"
			},
			{
				souraName: "الإنشقاق",
				startPage: 589,
				type: 0,
				numberOfPhrases: "25"
			},
			{
				souraName: "البروج",
				startPage: 590,
				type: 0,
				numberOfPhrases: "22"
			},
			{
				souraName: "الطارق",
				startPage: 591,
				type: 0,
				numberOfPhrases: "17"
			},
			{
				souraName: "الأعلى",
				startPage: 591,
				type: 0,
				numberOfPhrases: "19"
			},
			{
				souraName: "الغاشية",
				startPage: 592,
				type: 0,
				numberOfPhrases: "26"
			},
			{
				souraName: "الفجر",
				startPage: 593,
				type: 0,
				numberOfPhrases: "30"
			},
			{
				souraName: "البلد",
				startPage: 594,
				type: 0,
				numberOfPhrases: "20"
			},
			{
				souraName: "الشمس",
				startPage: 595,
				type: 0,
				numberOfPhrases: "15"
			},
			{
				souraName: "الليل",
				startPage: 595,
				type: 0,
				numberOfPhrases: "21"
			},
			{
				souraName: "الضحي",
				startPage: 596,
				type: 0,
				numberOfPhrases: "11"
			},
			{
				souraName: "الشرح",
				startPage: 596,
				type: 0,
				numberOfPhrases: "5"
			},
			{
				souraName: "التين",
				startPage: 597,
				type: 0,
				numberOfPhrases: "8"
			},
			{
				souraName: "العلق",
				startPage: 597,
				type: 0,
				numberOfPhrases: "19"
			},
			{
				souraName: "القدر",
				startPage: 598,
				type: 0,
				numberOfPhrases: "5"
			},
			{
				souraName: "البينة",
				startPage: 598,
				type: 1,
				numberOfPhrases: "8"
			},
			{
				souraName: "الزلزلة",
				startPage: 599,
				type: 1,
				numberOfPhrases: "8"
			},
			{
				souraName: "العاديات",
				startPage: 599,
				type: 0,
				numberOfPhrases: "11"
			},
			{
				souraName: "القارعة",
				startPage: 600,
				type: 0,
				numberOfPhrases: "11"
			},
			{
				souraName: "التكاثر",
				startPage: 600,
				type: 0,
				numberOfPhrases: "8"
			},
			{
				souraName: "العصر",
				startPage: 601,
				type: 0,
				numberOfPhrases: "3"
			},
			{
				souraName: "الهمزة",
				startPage: 601,
				type: 0,
				numberOfPhrases: "9"
			},
			{
				souraName: "الفيل",
				startPage: 601,
				type: 0,
				numberOfPhrases: "5"
			},
			{
				souraName: "قريش",
				startPage: 602,
				type: 0,
				numberOfPhrases: "4"
			},
			{
				souraName: "الماعون",
				startPage: 602,
				type: 0,
				numberOfPhrases: "7"
			},
			{
				souraName: "الكوثر",
				startPage: 602,
				type: 0,
				numberOfPhrases: "3"
			},
			{
				souraName: "الكافرون",
				startPage: 603,
				type: 0,
				numberOfPhrases: "6"
			},
			{
				souraName: "النصر",
				startPage: 603,
				type: 1,
				numberOfPhrases: "3"
			},
			{
				souraName: "المسد",
				startPage: 603,
				type: 0,
				numberOfPhrases: "5"
			},
			{
				souraName: "الإخلاص",
				startPage: 604,
				type: 0,
				numberOfPhrases: "4"
			},
			{
				souraName: "الفلق",
				startPage: 604,
				type: 0,
				numberOfPhrases: "5"
			},
			{
				souraName: "الناس",
				startPage: 604,
				type: 0,
				numberOfPhrases: "6"
			}
		]
	}]