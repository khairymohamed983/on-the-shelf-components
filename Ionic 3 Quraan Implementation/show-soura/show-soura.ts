import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { PARTS } from '../../providers/quraan-parts';
import { SURAH } from '../../providers/surah';

@IonicPage()
@Component({
  selector: 'page-show-soura',
  templateUrl: 'show-soura.html',
})
export class ShowSouraPage {

  lstsoura: any[];
  lstParts: any[];
  baseUrl: string = 'http://quran.ksu.edu.sa/ayat/safahat1/'
  //  'http://whisperstalk.com/Quran/';
  current: number;
  start: number = 1;
  end: number = 604;
  title: string = '';
  // partName: string = '';
  img: string;
  isBookmArked: boolean;
  LOADED = false;
  constructor(public navCtrl: NavController, public navParams: NavParams, public storage: Storage, public toast: ToastController) {
    this.current = navParams.get('index');
    this.title = navParams.get('title');
    if (!this.current) {
      this.current = 1;
    }
    this.checkCurrentBookmarked();
    let cur = this.getCurrent();
    this.img = this.baseUrl + cur + '.png';
    this.lstParts = PARTS;
    this.lstsoura = SURAH;
  }

  checkCurrentBookmarked() {
    this.storage.get('qurbookmarks').then((bookmarks) => {
      let bookmarkLst = bookmarks || [];
      this.isBookmArked = false;
      for (let i = 0; i < bookmarkLst.length; i++) {
        const element = bookmarkLst[i];
        if (element.page == this.current) {
          this.isBookmArked = true;
          break;
        }
      }
    });
  }
  ngAfterViewInit() {

  }

  goNext() {
    this.LOADED = false;
    let next = this.getNext();
    if (next) {
      this.img = this.baseUrl + next + '.png';
      this.checkCurrentBookmarked();
      this.setTitle();
    }
  }

  gotToBokkmarks() {
    this.navCtrl.push('QuraanBookmarksPage');
  }

  presentToast(message) {
    let toast = this.toast.create({
      message: message,
      duration: 3000,
      position: 'top'
    });
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
    toast.present();
  }

  bookMark() {
    this.storage.get('qurbookmarks').then((bookmarks) => {
      let bookmarkLst = bookmarks || [];
      let newObject = { page: this.current, soura: this.title };
      let found = false;
      for (let i = 0; i < bookmarkLst.length; i++) {
        const element = bookmarkLst[i];
        if (element.page == newObject.page) {
          found = true;
          break;
        }
      }
      if (found) {
        this.presentToast('هذه الصفحة موجوده بالفعل في الفواصل الخاصة بك .');
      } else {
        bookmarkLst.push(newObject);
        this.storage.set('qurbookmarks', bookmarkLst).then((bookmarks) => {
          this.presentToast('تم اضافة الصفحة الي الفواصل');
          this.checkCurrentBookmarked();
        });
      }
    }).catch((ex) => {
      console.log(ex);
    });
  }

  goPrev() {
    this.LOADED = false;
    let prev = this.getPrev();
    if (prev) {
      this.img = this.baseUrl + prev + '.png';
      this.setTitle();
      this.checkCurrentBookmarked();
    }
  }

  setTitle() {
    for (let i = 0; i < this.lstsoura.length; i++) {
      const element = this.lstsoura[i];
      if (this.current >= element.startPage && this.current <= element.endPage) {
        this.title = element.souraName;
        return;
      }
    }
  }

  onImageLoad(event) {
    this.LOADED = true;
  }

  Back() {
    this.navCtrl.pop()
  }

  getCurrent(): string {
    return this.current.toString();
    // return (this.current < 10 ? '00' + this.current : (this.current < 100 ? '0' + this.current : this.current.toString()));
  }

  getNext(): string {
    let next = this.current + 1;
    this.current = next;
    // return next <= this.end ? (next < 10 ? '00' + next : (next < 100 ? '0' + next : next.toString())) : '';
    return next <= this.end ? next.toString() : '';
  }

  getPrev(): string {
    let prev = this.current - 1;
    this.current = prev;
    // return prev >= this.start ? (prev < 10 ? '00' + prev : (prev < 100 ? '0' + prev : prev.toString())) : '';
    return prev >= this.start ? prev.toString() : '';
  }
}
