import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ShowSouraPage } from './show-soura';
import { IonicImageLoader } from 'ionic-image-loader';

@NgModule({
  declarations: [
   ShowSouraPage,
  ],
  imports: [
    IonicPageModule.forChild(ShowSouraPage),
    IonicImageLoader
  ],entryComponents:[
    ShowSouraPage
  ]
})

export class ShowSouraPageModule {}
