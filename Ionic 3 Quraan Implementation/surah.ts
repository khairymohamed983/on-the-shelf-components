export let SURAH = 
[
    {
           souraName: "الفاتحه",
           startPage: 1,
           endPage: 1,
           type: 0,
           numberOfPhrases: "7"
   },
       {
           souraName: "البقرة",
           startPage: 2,
           endPage: 49,
           type: 1,
           numberOfPhrases: "286"
   }, {
           souraName: "آل عمران",
           startPage: 50,
           endPage: 76,
           type: 1,
           numberOfPhrases: "176"
   }, {
           souraName: "النساء",
           startPage: 77,
           endPage: 106,
           type: 1,
           numberOfPhrases: "176"
   },
       {
           souraName: "المائدة",
           startPage: 106,
           endPage: 127,
           type: 1,
           numberOfPhrases: "120"
   },
       {
           souraName: "الأنعام",
           startPage: 128,
           endPage: 165,
           type: 0,
           numberOfPhrases: "165"
   }, {
           souraName: "الأعراف",
           startPage: 151,
           endPage: 186,
           type: 0,
           numberOfPhrases: "206"
   }, {
           souraName: "الأنفال",
           startPage: 177,
           endPage: 15,
           type: 1,
           numberOfPhrases: "75"
   },
       {
           souraName: "التوبة",
           startPage: 187,
           endPage: 207,
           type: 1,
           numberOfPhrases: "129"
   },
       {
           souraName: "يونس",
           startPage: 208,
           endPage: 220,
           type: 0,
           numberOfPhrases: "109"
   }, {
           souraName: "هود",
           startPage: 221,
           endPage: 234,
           type: 0,
           numberOfPhrases: "123"
   }, {
           souraName: "يوسف",
           startPage: 235,
           endPage: 248,
           type: 0,
           numberOfPhrases: "111"
   },
       {
           souraName: "الرعد",
           startPage: 249,
           endPage: 254,
           type: 1,
           numberOfPhrases: "43"
   },
       {
           souraName: "إبراهيم",
           startPage: 255,
           endPage: 261,
           type: 0,
           numberOfPhrases: "52"
   }, {
           souraName: "الحجر",
           startPage: 262,
           endPage: 266,
           type: 0,
           numberOfPhrases: "99"
   }, {
           souraName: "النحل",
           startPage: 267,
           endPage: 281,
           type: 0,
           numberOfPhrases: "128"
   },
   
       {
           souraName: "الإسراء",
           startPage: 282,
           endPage: 292,
           type: 1,
           numberOfPhrases: "111"
   },
       {
           souraName: "الكهف",
           startPage: 293,
           endPage: 304,
           type: 1,
           numberOfPhrases: "110"
   }, {
           souraName: "مريم",
           startPage: 305,
           endPage: 311,
           type: 1,
           numberOfPhrases: "98"
   }, {
           souraName: "طه",
           startPage: 312,
           endPage: 321,
           type: 1,
           numberOfPhrases: "135"
   },
    {
           souraName: "الأنبياء",
           startPage: 322,
           endPage: 331,
           type: 1,
           numberOfPhrases: "112"
   },
       {
           souraName: "الحج",
           startPage: 332,
           endPage: 341,
           type: 1,
           numberOfPhrases: "78"
   }, {
           souraName: "المؤمنون",
           startPage: 342,
           endPage: 349,
           type: 0,
           numberOfPhrases: "118"
   }, {
           souraName: "النور	",
           startPage: 350,
           endPage: 358,
           type: 0,
           numberOfPhrases: "64"
   }, {
           souraName: "الفرقان",
           startPage: 359,
           endPage: 366,
           type: 0,
           numberOfPhrases: "77"
   },
       {
           souraName: "الشعراء",
           startPage: 367,
           endPage: 376	,
           type: 0,
           numberOfPhrases: "227	"
   }, {
           souraName: "النمل	",
           startPage: 377,
           endPage: 384,
           type: 0,
           numberOfPhrases: "93"
   }, {
           souraName: "القصص	",
           startPage: 385,
           endPage: 395	,
           type: 0,
           numberOfPhrases: "88"
   },
   
       {
           souraName: "العنكبوت",
           startPage: 396,
           endPage: 403	,
           type: 0,
           numberOfPhrases: "69"
   },
       {
           souraName: "الروم",
           startPage: 404,
           endPage: 410,
           type: 0,
           numberOfPhrases: "60"
   }, {
           souraName: "لقمان",
           startPage: 411,
           endPage: 414,
           type: 0,
           numberOfPhrases: "34"
   }, {
           souraName: "السجدة",
           startPage: 415,
           endPage: 417	,
           type: 0,
           numberOfPhrases: "30"
   },
    {
           souraName: "الأحزاب",
           startPage: 418,
           endPage: 427	,
           type: 0,
           numberOfPhrases: "73"
   },
       {
           souraName: "سبأ",
           startPage: 428,
           endPage: 433	,
           type: 0,
           numberOfPhrases: "54"
   }, {
           souraName: "فاطر",
           startPage: 434,
           endPage: 439,
           type: 0,
           numberOfPhrases: "45"
   }, {
           souraName: "يس",
           startPage: 440,
           endPage: 445	,
           type: 0,
           numberOfPhrases: "83"
   },
   
       {
           souraName: "الصافات",
           startPage: 446,
           endPage: 452	,
           type: 0,
           numberOfPhrases: "181"
   },
       {
           souraName: "ص	",
           startPage: 453,
           endPage: 457	,
           type: 0,
           numberOfPhrases: "88"
   }, {
           souraName: "الزمر	",
           startPage: 458,
           endPage: 466,
           type: 0,
           numberOfPhrases: "75"
   }, {
           souraName: "غافر",
           startPage: 467,
           endPage: 476	,
           type: 0,
           numberOfPhrases: "85"
   },
       {
           souraName: "فصلت",
           startPage: 477,
           endPage: 482,
           type: 0,
           numberOfPhrases: "54"
   },
       {
           souraName: "الشورى",
           startPage: 483,
           endPage: 488,
           type: 0,
           numberOfPhrases: "53"
   }, {
           souraName: "الزخرف",
           startPage: 489,
           endPage: 495,
           type: 0,
           numberOfPhrases: "89"
   }, {
           souraName: "الدخان",
           startPage: 496,
           endPage: 498,
           type: 0,
           numberOfPhrases: "59"
   },
   
       {
           souraName: "الجاثية",
           startPage: 499,
           endPage: 501,
           type: 0,
           numberOfPhrases: "37"
   },
       {
           souraName: "الأحقاف",
           startPage: 502,
           endPage: 506 ,
           type: 0,
           numberOfPhrases: "35"
   }, {
           souraName: "محمد",
           startPage: 507,
           endPage: 510	,
           type: 0,
           numberOfPhrases: "38"
   }, {
           souraName: "الفتح	",
           startPage: 511,
           endPage: 514,
           type: 0,
           numberOfPhrases: "29"
   },
       {
           souraName: "الحجرات",
           startPage: 515,
           endPage: 517,
           type: 0,
           numberOfPhrases: "18"
   },
       {
           souraName: "ق",
           startPage: 518,
           endPage: 519	,
           type: 0,
           numberOfPhrases: "45"
   }, {
           souraName: "الذاريات	",
           startPage: 520,
           endPage: 522,
           type: 0,
           numberOfPhrases: "60"
   }, {
           souraName: "الطور	",
           startPage: 523,
           endPage: 525,
           type: 0,
           numberOfPhrases: "49"
   }, {
           souraName: "النجم",
           startPage: 526,
           endPage: 527,
           type: 0,
           numberOfPhrases: "62"
   },
       {
           souraName: "القمر",
           startPage: 528,
           endPage: 530,
           type: 0,
           numberOfPhrases: "55"
   }, {
           souraName: "الرحمن",
           startPage: 531,
           endPage: 533,
           type: 0,
           numberOfPhrases: "78"
   }, {
           souraName: "الواقعة",
           startPage: 534,
           endPage: 536,
           type: 0,
           numberOfPhrases: "96"
   },
   
       {
           souraName: "الحديد",
           startPage: 537,
           endPage: 15,
           type: 0,
           numberOfPhrases: "29"
   },
       {
           souraName: "المجادلة",
           startPage: 545,
           endPage: 548,
           type: 0,
           numberOfPhrases: "22"
   }, {
           souraName: "الحشر",
           startPage: 549,
           endPage: 550,
           type: 0,
           numberOfPhrases: "24"
   }, {
           souraName: "الممتحنة",
           startPage: 551,
           endPage: 552,
           type: 0,
           numberOfPhrases: "13"
   },
   
       {
           souraName: "الصف",
           startPage: 553,
           endPage: 553,
           type: 0,
           numberOfPhrases: "14"
   },
       {
           souraName: "الجمعة",
           startPage: 0,
           endPage: 15,
           type: 0,
           numberOfPhrases: "11"
   }, {
           souraName: "المنافقون",
           startPage: 554,
           endPage: 555,
           type: 0,
           numberOfPhrases: "11"
   }, {
           souraName: "التغابن",
           startPage: 556,
           endPage: 557,
           type: 0,
           numberOfPhrases: "18"
   },
   
   {
           souraName: "الطلاق",
           startPage: 558,
           endPage: 559,
           type: 0,
           numberOfPhrases: "12"
   },
       {
           souraName: "التحريم",
           startPage: 560,
           endPage: 561,
           type: 0,
           numberOfPhrases: "30"
   },{
           souraName: "الملك",
           startPage: 562,
           endPage: 563,
           type: 0,
           numberOfPhrases: "30"
   }, {
           souraName: "القلم",
           startPage: 564,
           endPage: 565,
           type: 0,
           numberOfPhrases: "52"
   }, {
           souraName: "الحاقة",
           startPage: 566,
           endPage: 567,
           type: 0,
           numberOfPhrases: "52"
   },
   {
           souraName: "المعارج",
           startPage: 568,
           endPage: 599,
           type: 0,
           numberOfPhrases: "44"
   },
       {
           souraName: "نوح",
           startPage: 570,
           endPage: 571,
           type: 0,
           numberOfPhrases: "28"
   }, {
           souraName: "الجن",
           startPage: 572,
           endPage: 573,
           type: 0,
           numberOfPhrases: "28"
   }, {
           souraName: "المزمل",
           startPage: 574,
           endPage: 574,
           type: 0,
           numberOfPhrases: "20"
   },
   
   
       {
           souraName: "المدثر",
           startPage: 575,
           endPage: 576,
           type: 0,
           numberOfPhrases: "56"
   },
       {
           souraName: "القيامة",
           startPage: 577,
           endPage: 577,
           type: 0,
           numberOfPhrases: "40"
   }, {
           souraName: "الإنسان",
           startPage: 578,
           endPage: 579,
           type: 0,
           numberOfPhrases: "31"
   }, {
           souraName: "المرسلات	",
           startPage: 580,
           endPage: 581,
           type: 0,
           numberOfPhrases: "50"
   },
       {
           souraName: "النبأ",
           startPage: 582,
           endPage: 583,
           type: 0,
           numberOfPhrases: "40"
   },
       {
           souraName: "النازعات",
           startPage: 583,
           endPage: 584,
           type: 0,
           numberOfPhrases: "46"
   }, {
           souraName: "عبس",
           startPage: 585,
           endPage: 585,
           type: 0,
           numberOfPhrases: "42"
   }, 
       {
           souraName: "التكوير",
           startPage: 586,
           endPage: 586,
           type: 0,
           numberOfPhrases: "29"
   },
       {
           souraName: "الإنفطار",
           startPage: 586,
           endPage: 586,
           type: 0,
           numberOfPhrases: "29"
   }, {
           souraName: "المطففين",
           startPage: 587,
           endPage: 588,
           type: 0,
           numberOfPhrases: "36"
   }, {
           souraName: "الإنشقاق",
           startPage: 589,
           endPage: 589,
           type: 0,
           numberOfPhrases: "25"
   },
   
       {
           souraName: "البروج",
           startPage: 590,
           endPage: 590,
           type: 0,
           numberOfPhrases: "22"
   },
    {
           souraName: "الطارق",
           startPage: 591,
           endPage: 591,
           type: 0,
           numberOfPhrases: "17"
   },
   
       {
           souraName: "الاعلى",
           startPage: 591,
           endPage: 591,
           type: 0,
           numberOfPhrases: "19"
   },
       {
           souraName: "الغاشية",
           startPage: 592,
           endPage: 592,
           type: 0,
           numberOfPhrases: "26"
   }, {
           souraName: "الفجر",
           startPage: 593,
           endPage: 593,
           type: 0,
           numberOfPhrases: "30"
   }, {
           souraName: "البلد",
           startPage: 594,
           endPage: 594,
           type: 0,
           numberOfPhrases: "20"
   },
   
       {
           souraName: "الشمس",
           startPage: 595,
           endPage: 595,
           type: 0,
           numberOfPhrases: "15"
   },
       {
           souraName: "الليل",
           startPage: 595,
           endPage: 595,
           type: 0,
           numberOfPhrases: "21"
   }, {
           souraName: "الضحى",
           startPage: 596,
           endPage: 596,
           type: 0,
           numberOfPhrases: "11"
   }, {
           souraName: "الشرح",
           startPage: 596,
           endPage: 596,
           type: 0,
           numberOfPhrases: "8"
   },
   
       {
           souraName: "التين",
           startPage: 597,
           endPage: 597,
           type: 0,
           numberOfPhrases: "8"
   },
       {
           souraName: "العلق",
           startPage: 597,
           endPage: 597,
           type: 0,
           numberOfPhrases: "19"
   }, {
           souraName: "القدر",
           startPage: 598,
           endPage: 598,
           type: 0,
           numberOfPhrases: "5"
   }, {
           souraName: "البينة",
           startPage: 598,
           endPage: 598,
           type: 0,
           numberOfPhrases: "8"
   },
   
       {
           souraName: "الزلزله",
           startPage: 599,
           endPage: 599,
           type: 0,
           numberOfPhrases: "8"
   },
       {
           souraName: "العاديات",
           startPage: 599,
           endPage: 599,
           type: 0,
           numberOfPhrases: "11"
   }, {
           souraName: "القارعة",
           startPage: 600,
           endPage: 600,
           type: 0,
           numberOfPhrases: "11"
   }, {
           souraName: "التكاثر",
           startPage: 600,
           endPage: 600,
           type: 0,
           numberOfPhrases: "8"
   },
   
       {
           souraName: "العصر",
           startPage: 601,
           endPage: 601,
           type: 0,
           numberOfPhrases: "3"
   },
       {
           souraName: "الهمزة",
           startPage: 601,
           endPage: 601,
           type: 0,
           numberOfPhrases: "9"
   }, {
           souraName: "الفيل",
           startPage: 601,
           endPage: 601,
           type: 0,
           numberOfPhrases: "5"
   }, {
           souraName: "قريش",
           startPage: 602,
           endPage: 602,
           type: 0,
           numberOfPhrases: "4"
   },
   
   {
           souraName: "الماعون",
           startPage: 602,
           endPage: 602,
           type: 0,
           numberOfPhrases: "7"
   },
       {
           souraName: "الكوثر",
           startPage: 602,
           endPage: 602,
           type: 0,
           numberOfPhrases: "3"
   }, {
           souraName: "الكافرون",
           startPage: 603,
           endPage: 603,
           type: 0,
           numberOfPhrases: "6"
   }, {
           souraName: "النصر",
           startPage: 603,
           endPage: 603,
           type: 0,
           numberOfPhrases: "3"
   },
   {
           souraName: "المسد",
           startPage: 603,
           endPage: 603,
           type: 0,
           numberOfPhrases: "5"
   },
       {
           souraName: "الإخلاص",
           startPage: 604,
           endPage: 604,
           type: 0,
           numberOfPhrases: "4"
   }, {
           souraName: "الفلق",
           startPage: 604,
           endPage: 604,
           type: 0,
           numberOfPhrases: "5"
   }, {
           souraName: "الناس",
           startPage: 604,
           endPage: 604,
           type: 0,
           numberOfPhrases: "6"
   },
   
   
   ]