import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SURAH } from '../../providers/surah';
import { PARTS } from '../../providers/quraan-parts';

@IonicPage()

@Component({
  selector: 'page-quraan',
  templateUrl: 'quraan.html',
})

export class QuraanPage {
  Divides: string = 'Parts'

  lstsoura: any[];
  lstParts: any[];

  selectedPart :any ;

  constructor(public navCtrl: NavController, public navParams: NavParams,
  ) {
    this.lstsoura = SURAH;
    this.lstParts = PARTS;

    let SelectedPart = navParams.get("selectedPart");
    if(SelectedPart){
      this.selectedPart = SelectedPart;
      this.lstsoura = SelectedPart.sour;
      this.Divides = 'Soura';
    }
  }

  gotoshowsourapge(data) {
    // debugger;
    this.navCtrl.push('ShowSouraPage', { index: data.startPage, title: data.souraName});
  }

  gotToBokkmarks() {
    this.navCtrl.push('QuraanBookmarksPage');
  }

  viewPartSour(part){
    this.navCtrl.push("QuraanPage" , {selectedPart: part});
  }

  viewsourList (){
    this.lstsoura = SURAH;
  }


 
  Back() {
       this.navCtrl.pop()

  }

  ionViewWillLeave() {  
   

  }

  gotaboutpage() {
    this.navCtrl.push('AboutUsPage');
  }

}
