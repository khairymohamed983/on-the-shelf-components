import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { QuraanPage } from './quraan';


@NgModule({
  declarations: [
   QuraanPage,
  ],
  imports: [
     IonicPageModule.forChild(QuraanPage),
  ],
  entryComponents:[
    QuraanPage
  ]
})
export class QuraanPageModule {}
