import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
//  import { Calendar } from '@ionic-native/calendar';
import HijriDate from 'hijri-date/lib/safe';
// import { Http } from '@angular/http';
import { HTTP } from "@ionic-native/http";
@IonicPage()
@Component({
  selector: 'page-calender',
  templateUrl: 'calender.html',
})
export class CalenderPage {

  date: any;
  hijriDate: HijriDate;

  daysInThisMonth: any;
  hijriDaysInThisMonth: any = [];

  daysInLastMonth: any;
  hijridaysInLastMonth: any = [];

  daysInNextMonth: any;
  hijridaysInNextMonth: any;

  monthNames: string[];
  hijrimonthNames: string[];

  currentMonth: any;
  hijricurrentMonth: any;

  currentYear: any;
  hijricurrentYear: any;

  currentDate: any;
  hijricurrentDate: any;

  currentDay: any;
  CALENDER: string = 'calenderr';

  constructor( 
    public navCtrl: NavController,
    public http: HTTP) { }

  ionViewWillEnter() {
    this.date = new Date();
    this.monthNames = ["يناير", "فبراير", "مارس", "إبريل", "مايو", "يونيو", "يوليو", "أغسطس", "سبتمبر", "أكتوبر", "نوفمبر", "ديسيمبر"];
    this.hijrimonthNames = ["محرم", "صفر", "ربيع الأول", "ربيع الآخر", "جماد الأول", "جماد الآخر", "رجب", "شعبان", "رمضان", "شوال", "ذو القعدة", "ذو الحجة"];
    // console.log(this.hijriDate);
    this.hijriDate = new HijriDate();
    console.log(this.hijriDate);

    this.getDaysOfMonth();
    console.log(this.date.getDate() + '-' + (this.date.getMonth() + 1) + '-' + this.date.getFullYear());

    this.http.get('http://api.aladhan.com/gToH?date=' + this.date.getDate() + '-' + (this.date.getMonth() + 1) + '-' + this.date.getFullYear() , {} ,{})
      .then((d) => {
        let res = JSON.parse(d.data);
        this.currentDay = Number(res.data.hijri.day);
        this.getHihriDaysOfMonth();
      })
  }

  // milady Date calender

  getDaysOfMonth() {
    this.daysInThisMonth = new Array();
    this.daysInLastMonth = new Array();
    this.daysInNextMonth = new Array();
    this.currentMonth = this.monthNames[this.date.getMonth()];
    this.currentYear = this.date.getFullYear();
    if (this.date.getMonth() === new Date().getMonth()) {
      this.currentDate = new Date().getDate();
      console.log('current Date ', this.currentDate);

    } else {
      this.currentDate = 999;
    }

    var firstDayThisMonth = new Date(this.date.getFullYear(), this.date.getMonth(), 1).getDay();
    var prevNumOfDays = new Date(this.date.getFullYear(), this.date.getMonth(), 0).getDate();
    for (var i = prevNumOfDays - (firstDayThisMonth - 1); i <= prevNumOfDays; i++) {
      this.daysInLastMonth.push('');
    }

    var thisNumOfDays = new Date(this.date.getFullYear(), this.date.getMonth() + 1, 0).getDate();
    for (var j = 0; j < thisNumOfDays; j++) {
      this.daysInThisMonth.push(j + 1);
    }

    var lastDayThisMonth = new Date(this.date.getFullYear(), this.date.getMonth() + 1, 0).getDay();
    // var nextNumOfDays = new Date(this.date.getFullYear(), this.date.getMonth()+2, 0).getDate();
    for (var k = 0; k < (6 - lastDayThisMonth); k++) {
      this.daysInNextMonth.push('');
    }
    var totalDays = this.daysInLastMonth.length + this.daysInThisMonth.length + this.daysInNextMonth.length;
    if (totalDays < 36) {
      for (var l = (7 - lastDayThisMonth); l < ((7 - lastDayThisMonth) + 7); l++) {
        this.daysInNextMonth.push('');
      }
    }
  }
  gotaboutpage() {
    this.navCtrl.push('AboutUsPage');
  }


  GotoSettingPage() {
    this.navCtrl.push('SettingPage')
  }
  
  goToLastMonth() {
    this.date = new Date(this.date.getFullYear(), this.date.getMonth(), 0);
    this.getDaysOfMonth();
  }

  goToNextMonth() {
    this.date = new Date(this.date.getFullYear(), this.date.getMonth() + 2, 0);
    this.getDaysOfMonth();
  }

  weekdays = [
    "السبت",
    "الاحد",
    "الاثنين",
    "الثلاثاء",
    "الاربعاء",
    "الخميس",
    "الجمعة"];

  getHihriDaysOfMonth() {
    this.hijriDaysInThisMonth = new Array();
    this.hijridaysInLastMonth = new Array();

    this.http.get('http://api.aladhan.com/hToGCalendar/' + this.hijriDate.getMonth() + '/' + this.hijriDate.getFullYear() , {} ,{})
      .then((d) => {
        let data1  =JSON.parse(d.data).data;
        this.hijricurrentMonth = data1[0].hijri.month.ar;
        this.hijricurrentYear = data1[0].hijri.year;

        let firstDay = data1[0].hijri.weekday.ar;
        let firstDayIndex = this.weekdays.indexOf(firstDay);
        for (let i = 0; i < firstDayIndex; i++) {
          this.hijridaysInLastMonth.push('');
        }
        console.log(this.hijridaysInLastMonth);
        for (var j = 0; j < data1.length; j++) {
          this.hijriDaysInThisMonth.push(j + 1);
        }
        console.log(data1);
      });

    if (this.hijriDate.getMonth() === new HijriDate().getMonth()) {
      this.hijricurrentDate = this.currentDay;
    } else {
      this.hijricurrentDate = 999;
    }
    console.log(this.hijricurrentDate);
  }

  goToLastHijriMonth() {
    let prevMonth = this.hijriDate.getMonth() - 1;
    let prevYear = this.hijriDate.getFullYear();

    if (this.hijriDate.getMonth() == 1) {
      prevMonth = 12;
      prevYear--;
    }

    this.hijriDate = new HijriDate(prevYear, prevMonth  , 0);
    this.getHihriDaysOfMonth();
  }

  goToNextHijriMonth() {
    let nextMonth = this.hijriDate.getMonth() + 1;
    let nextYear = this.hijriDate.getFullYear();
    if (this.hijriDate.getMonth() == 12) {
      nextMonth = 1;
      nextYear++;
    }
    this.hijriDate = new HijriDate(nextYear, nextMonth, 0);
    this.getHihriDaysOfMonth();
  }
  Back() {
    this.navCtrl.pop();
  }
}
