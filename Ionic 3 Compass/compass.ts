import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { KhairyCompassEngineProvider } from '../../providers/khairy-compass-engine/khairy-compass-engine';
import { DeviceOrientation } from "@ionic-native/device-orientation";
import { state, style, trigger, transition, animate } from '@angular/animations';
// import { DomSanitizer } from '@angular/platform-browser';
import { Geolocation } from '@ionic-native/geolocation';

@IonicPage()

@Component({
  selector: 'page-compass',
  templateUrl: 'compass.html',
  animations: [trigger('niddleMoving', [
    state('stop', style({ transform: 'rotate({{targetDegree}}deg)' }), {
      params: {
        targetDegree: 0
      }
    }),
    state('move', style({ transform: 'rotate({{targetDegree}}deg)' }), {
      params: {
        targetDegree: 0
      }
    }),
    transition('stop => move', animate('{{velocity}}ms ease-out'))
  ]),
  trigger('ka3baMovement', [
    state('init', style({
      transform: 'rotate(-90deg) translateX(130px) rotate(90deg)'
    })),
    state('locate', style({
      transform: 'rotate({{ka3baBeering}}deg) translateX(130px) rotate({{transfromation}}deg)'
    }), {
        params: {
          ka3baBeering: 0,
          transfromation: 0
        }
      }),
    transition('init=>locate', animate("2s"))
  ])
  ]
})

export class CompassPage {

  // vars 
  ka3baMovement: any;
  ka3baBearing: any;
  niddleMovement: any;
  subscription: any;
  prefDegree = 0;

  // composite vars
  ka3baLocation = {
    lat: 21.423899,
    long: 39.825509
  };
  currentLocation = {
    lat: 0,
    long: 0
  }


  getniddleMovement(): any { return this.niddleMovement; }
  getKa3baMovement(): any { return this.ka3baMovement; }

  setCompassNiddle(degree) {
    console.log(degree);
    this.niddleMovement = {
      value: "move",
      params: {
        targetDegree: degree,
        velocity: degree * 10
      }
    }
  }

  setKa3baLocation(state = "init") {
    this.ka3baMovement = {
      value: state,
      params: {
        ka3baBeering: this.ka3baBearing,
        transfromation: this.ka3baBearing
      }
    }
  }

  constructor(public navCtrl: NavController,
    private kmCompass: KhairyCompassEngineProvider,
    // private sanitizer: DomSanitizer,
    private geolocation: Geolocation,
    public navParams: NavParams,
    private orientation: DeviceOrientation) {

    this.setCompassNiddle(0);
    this.setKa3baLocation();

    this.initAndWatchGeoLocationToLocateKa3ba();
    this.initAndWatchDeviceOrientationToDetermineDirections();
  }

  initAndWatchDeviceOrientationToDetermineDirections() {
    this
      .orientation
      .getCurrentHeading()
      .then((data) => {
        this.setCompassNiddle(data.magneticHeading)
      }, (error: any) => {
        console.log(error);
      });

    this.subscription = this
      .orientation
      .watchHeading()
      .subscribe((data) => {
        this.setCompassNiddle(data.magneticHeading);
      });
  }


  initAndWatchGeoLocationToLocateKa3ba() {
    this.geolocation.getCurrentPosition().then((resp) => {
      this.currentLocation.lat = resp.coords.latitude;
      this.currentLocation.long = resp.coords.longitude;
      this.locateKa3baOnCompass();
    }).catch((error) => {
      console.log('Error getting location', error);
    });

    let watch = this.geolocation.watchPosition();
    watch.subscribe((data) => {
      if (data.coords.latitude && data.coords.longitude) {
        this.currentLocation.lat = data.coords.latitude;
        this.currentLocation.long = data.coords.longitude;
        this.locateKa3baOnCompass();
      }
    });
  }

  gotaboutpage() {
    this.navCtrl.push('AboutUsPage');
  }

  locateKa3baOnCompass() {
    this.ka3baBearing = this.kmCompass.bearingTo(this.currentLocation, this.ka3baLocation) -90 || -90;
    console.log('the ka3ba bearing from the north is :: ', this.ka3baBearing);
    this.setKa3baLocation('locate');
  }

  ionViewDidLoad() {

  }

  Back() {
    this.navCtrl.pop();
  }

}