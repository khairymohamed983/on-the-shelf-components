import { Injectable } from '@angular/core';
@Injectable()
export class KhairyCompassEngineProvider {

  constructor() {
    console.log('Hello to the super  KhairyCompassEngineProvider Provider');
  }

  bearingTo(current, point) {
    var φ1 = this.toRadians(current.lat), φ2 = this.toRadians(point.lat);
    var Δλ = this.toRadians(point.long - current.long);
    var y = Math.sin(Δλ) * Math.cos(φ2);
    var x = Math.cos(φ1) * Math.sin(φ2) - Math.sin(φ1) * Math.cos(φ2) * Math.cos(Δλ);
    var θ = Math.atan2(y, x);
    return (this.toDegree(θ) + 360) % 360;
  }

  finalBearingTo(current, point) {
    debugger;
    // get initial bearing from destination point to this point & reverse it by adding 180°
    return (this.bearingTo(current, point) + 180) % 360;
  }

  toDegree(val) {
    return val * 180 / Math.PI;
  }

  toRadians(val) {
    return val * Math.PI / 180;;
  }
}


