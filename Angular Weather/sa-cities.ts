export let CITIES =
    [
        {
            nameEn: "Ash Shubramīyah",
            name: "الشبرمية",
            id: 108210,
            country: "SA",
            coord: {
                lon: 44.130001,
                lat: 24.253611
            }
        },
        {
            nameEn: "Qaşr al Quraynah",
            name: "قصر القرينه",
            id: 108947,
            country: "SA",
            coord: {
                lon: 46.421291,
                lat: 24.46747
            }
        },
        {
            nameEn: "Al Fāḑilī",
            name: "الفاضلي",
            id: 392343,
            country: "SA",
            coord: {
                lon: 49.174511,
                lat: 26.98016
            }
        },
        {
            nameEn: "Tubarjal",
            name: "طبرجل",
            id: 101631,
            country: "SA",
            coord: {
                lon: 38.21603,
                lat: 30.49987
            }
        },
        {
            nameEn: "Samitah",
            name: "سميته",
            id: 102451,
            country: "SA",
            coord: {
                lon: 42.944351,
                lat: 16.59601
            }
        },
        {
            nameEn: "An Nimas",
            name: "نيماس",
            id: 108617,
            country: "SA",
            coord: {
                lon: 42.120098,
                lat: 19.14547
            }
        },
        {
            nameEn: "Al Munayzilah",
            name: "المنايزة",
            id: 109059,
            country: "SA",
            coord: {
                lon: 49.666672,
                lat: 25.383329
            }
        },
        {
            nameEn: "Al Mubarraz",
            name: "المبرز",
            id: 109101,
            country: "SA",
            coord: {
                lon: 49.58083,
                lat: 25.41
            }
        },
        {
            nameEn: "Afif",
            name: "عفيف",
            id: 110250,
            country: "SA",
            coord: {
                lon: 42.91724,
                lat: 23.9065
            }
        },
        {
            nameEn: "Ad Dilam",
            name: "الدلم",
            id: 110314,
            country: "SA",
            coord: {
                lon: 47.162659,
                lat: 23.991461
            }
        },
        {
            nameEn: "Minţaqat ar Riyāḑ",
            name: "منطقة الرياض",
            id: 108411,
            country: "SA",
            coord: {
                lon: 45.333328,
                lat: 23.33333
            }
        },
        {
            nameEn: "Al Kharj",
            name: "الخرج",
            id: 109353,
            country: "SA",
            coord: {
                lon: 47.334599,
                lat: 24.15502
            }
        },
        {
            nameEn: "As Sulaymānīyah",
            name: "السليمانية",
            id: 108052,
            country: "SA",
            coord: {
                lon: 47.295559,
                lat: 24.12361
            }
        },
        {
            nameEn: "As Sahbā’",
            name: "الصحبة",
            id: 407308,
            country: "SA",
            coord: {
                lon: 47.379662,
                lat: 24.191019
            }
        },
        {
            nameEn: "Al Minţaqah ash Sharqīyah",
            name: "المنطقة الشرقية",
            id: 108241,
            country: "SA",
            coord: {
                lon: 51,
                lat: 22.5
            }
        },
        {
            nameEn: "‘Uşayfirāt",
            name: "يوسفرات",
            id: 391749,
            country: "SA",
            coord: {
                lon: 49.432049,
                lat: 26.10927
            }
        },
        {
            nameEn: "Umm Lajj",
            name: "املج",
            id: 100926,
            country: "SA",
            coord: {
                lon: 37.268501,
                lat: 25.021259
            }
        },
        {
            nameEn: "Turabah",
            name: "ترابة",
            id: 101322,
            country: "SA",
            coord: {
                lon: 41.633099,
                lat: 21.21406
            }
        },
        {
            nameEn: "Sabya",
            name: "صبيا",
            id: 102651,
            country: "SA",
            coord: {
                lon: 42.62537,
                lat: 17.1495
            }
        },
        {
            nameEn: "Az Zulfi",
            name: "الزلفي",
            id: 107781,
            country: "SA",
            coord: {
                lon: 44.815418,
                lat: 26.29945
            }
        },
        {
            nameEn: "Kingdom of Saudi Arabia",
            name: "المملكة العربية السعودية",
            id: 102358,
            country: "SA",
            coord: {
                lon: 45,
                lat: 25
            }
        },
        {
            nameEn: "Minţaqat Makkah",
            name: "منطقة مكة",
            id: 104514,
            country: "SA",
            coord: {
                lon: 41.333328,
                lat: 20.66667
            }
        },
        {
            nameEn: "Al Mudawwarah",
            name: "المدورة",
            id: 411338,
            country: "SA",
            coord: {
                lon: 39.962681,
                lat: 21.271589
            }
        },
        {
            nameEn: "Şumaymah",
            name: "السوميمة",
            id: 101752,
            country: "SA",
            coord: {
                lon: 39.157711,
                lat: 21.249649
            }
        },
        {
            nameEn: "Raḑwān",
            name: "راضوان",
            id: 409950,
            country: "SA",
            coord: {
                lon: 41.274559,
                lat: 22.09037
            }
        },
        {
            nameEn: "Ash Shurayf",
            name: "الشريف",
            id: 108190,
            country: "SA",
            coord: {
                lon: 39.282341,
                lat: 25.707081
            }
        },
        {
            nameEn: "Jeddah",
            name: "جدة",
            id: 105343,
            country: "SA",
            coord: {
                lon: 39.21917,
                lat: 21.516939
            }
        },
        {
            nameEn: "Riyadh",
            name: "الرياض",
            id: 108410,
            country: "SA",
            coord: {
                lon: 46.721851,
                lat: 24.687731
            }
        },
        {
            nameEn: "Al Jubayl",
            name: "الجبيل",
            id: 109435,
            country: "SA",
            coord: {
                lon: 49.65826,
                lat: 27.011221
            }
        },
        {
            nameEn: "Al Fayşalīyah",
            name: "الفيصلية",
            id: 400685,
            country: "SA",
            coord: {
                lon: 46.776981,
                lat: 24.64348
            }
        },
        {
            nameEn: "Taif",
            name: "الطائف",
            id: 107968,
            country: "SA",
            coord: {
                lon: 40.415829,
                lat: 21.270281
            }
        },
        {
            nameEn: "Ad Dammam",
            name: "الدمام",
            id: 110336,
            country: "SA",
            coord: {
                lon: 50.10326,
                lat: 26.43442
            }
        },
        {
            nameEn: "Ash Shu‘aybah",
            name: "الشعيبة",
            id: 389860,
            country: "SA",
            coord: {
                lon: 50.11528,
                lat: 26.433889
            }
        },
        {
            nameEn: "Ras Tanura",
            name: "رأس تنورة",
            id: 102891,
            country: "SA",
            coord: {
                lon: 50.159168,
                lat: 26.64389
            }
        },
        {
            nameEn: "Minţaqat Tabūk",
            name: "منطقة تابوك",
            id: 101627,
            country: "SA",
            coord: {
                lon: 37.333328,
                lat: 27.5
            }
        },
        {
            nameEn: "Tabuk",
            name: "تابوك",
            id: 101628,
            country: "SA",
            coord: {
                lon: 36.583328,
                lat: 28.383329
            }
        },
        {
            nameEn: "Mecca",
            name: "مكة",
            id: 104515,
            country: "SA",
            coord: {
                lon: 39.826111,
                lat: 21.42667
            }
        },
        {
            nameEn: "Minţaqat ‘Asīr",
            name: "منطقة عسير",
            id: 108179,
            country: "SA",
            coord: {
                lon: 43.166672,
                lat: 19.16667
            }
        },
        {
            nameEn: "Abha",
            name: "أبها",
            id: 110690,
            country: "SA",
            coord: {
                lon: 42.50528,
                lat: 18.216391
            }
        },
        {
            nameEn: "Minţaqat al Madīnah",
            name: "منطقة المدينة",
            id: 109224,
            country: "SA",
            coord: {
                lon: 39.666672,
                lat: 25.66667
            }
        },
        {
            nameEn: "Masāḩilī",
            name: "مسهلي",
            id: 104308,
            country: "SA",
            coord: {
                lon: 38.23246,
                lat: 24.027769
            }
        },
        {
            nameEn: "Madīnat Yanbu‘ aş Şinā‘īyah",
            name: "ينبع",
            id: 104625,
            country: "SA",
            coord: {
                lon: 38.227501,
                lat: 23.999439
            }
        },
        {
            nameEn: "Nimrān",
            name: "Nimrān",
            id: 103533,
            country: "SA",
            coord: {
                lon: 42.599998,
                lat: 20
            }
        },
        {
            nameEn: "Buraydah",
            name: "Buraydah",
            id: 107304,
            country: "SA",
            coord: {
                lon: 43.974972,
                lat: 26.325991
            }
        },
        {
            nameEn: "Abqaiq",
            name: "ابقيق",
            id: 107312,
            country: "SA",
            coord: {
                lon: 49.677608,
                lat: 25.937099
            }
        },
        {
            nameEn: "Al Qatif",
            name: "القطيف",
            id: 108927,
            country: "SA",
            coord: {
                lon: 50.024521,
                lat: 26.5208
            }
        },
        {
            nameEn: "Dhahran",
            name: "الظهران",
            id: 107797,
            country: "SA",
            coord: {
                lon: 50.135281,
                lat: 26.30324
            }
        },
        {
            nameEn: "Abā as Su‘ūd",
            name: "أبا صويل",
            id: 110737,
            country: "SA",
            coord: {
                lon: 44.099998,
                lat: 17.466669
            }
        },
        {
            nameEn: "Khamis Mushayt",
            name: "خميس مشيط",
            id: 105072,
            country: "SA",
            coord: {
                lon: 42.729172,
                lat: 18.306391
            }
        },
        {
            nameEn: "Minţaqat al Qaşīm",
            name: "منطقة القصيم",
            id: 108933,
            country: "SA",
            coord: {
                lon: 43.466671,
                lat: 27.08333
            }
        },
        {
            nameEn: "Ar Rass",
            name: "الرس",
            id: 108435,
            country: "SA",
            coord: {
                lon: 43.497299,
                lat: 25.86944
            }
        },
        {
            nameEn: "Medina",
            name: "المدينة المنورة",
            id: 109223,
            country: "SA",
            coord: {
                lon: 39.61417,
                lat: 24.468611
            }
        },
        {
            nameEn: "Minţaqat Jīzān",
            name: "منطقة جازان",
            id: 105298,
            country: "SA",
            coord: {
                lon: 42.666672,
                lat: 17.33333
            }
        },
        {
            nameEn: "Jizan",
            name: "جازان",
            id: 105299,
            country: "SA",
            coord: {
                lon: 42.551109,
                lat: 16.88917
            }
        },
        {
            nameEn: "Ḩafar al Bāţin",
            name: "حفر الباطن",
            id: 106297,
            country: "SA",
            coord: {
                lon: 45.970772,
                lat: 28.432791
            }
        },
        {
            nameEn: "Naghbī",
            name: "ناجبي",
            id: 103665,
            country: "SA",
            coord: {
                lon: 42.25,
                lat: 17.73333
            }
        },
        {
            nameEn: "Minţaqat Ḩā’il",
            name: "منطقة حائل",
            id: 106280,
            country: "SA",
            coord: {
                lon: 42.5,
                lat: 27.5
            }
        },
        {
            nameEn: "Hayil",
            name: "حائل",
            id: 106281,
            country: "SA",
            coord: {
                lon: 41.690731,
                lat: 27.521879
            }
        },
        {
            nameEn: "Al Awjam",
            name: "الأوجام",
            id: 110107,
            country: "SA",
            coord: {
                lon: 49.94331,
                lat: 26.56324
            }
        },
        {
            nameEn: "Al Ābār",
            name: "الابار",
            id: 411398,
            country: "SA",
            coord: {
                lon: 39.911419,
                lat: 21.09469
            }
        },
        {
            nameEn: "Yanbu al Bahr",
            name: "ينبع البحر",
            id: 100425,
            country: "SA",
            coord: {
                lon: 38.06374,
                lat: 24.089121
            }
        },
        {
            nameEn: "Al Wajh",
            name: "الوجه",
            id: 108773,
            country: "SA",
            coord: {
                lon: 36.45248,
                lat: 26.24551
            }
        },
        {
            nameEn: "Tamrah",
            name: "تمره",
            id: 101591,
            country: "SA",
            coord: {
                lon: 45.433941,
                lat: 20.40843
            }
        },
        {
            nameEn: "Turayf",
            name: "طريف",
            id: 101312,
            country: "SA",
            coord: {
                lon: 38.663738,
                lat: 31.67252
            }
        },
        {
            nameEn: "Al ‘Awājīyah",
            name: "العواجرية",
            id: 409515,
            country: "SA",
            coord: {
                lon: 40.54842,
                lat: 21.447689
            }
        },
        {
            nameEn: "Qārā",
            name: "قره",
            id: 103331,
            country: "SA",
            coord: {
                lon: 40.22694,
                lat: 29.87694
            }
        },
        {
            nameEn: "Sharūrah",
            name: "شرورة",
            id: 8019123,
            country: "SA",
            coord: {
                lon: 47.11488,
                lat: 17.48946
            }
        },
        {
            nameEn: "Minţaqat al Ḩudūd ash Shamālīyah",
            name: "منطقة الحدود الشمالية",
            id: 109579,
            country: "SA",
            coord: {
                lon: 42.666672,
                lat: 30
            }
        },
        {
            nameEn: "Arar",
            name: "عرعر",
            id: 108512,
            country: "SA",
            coord: {
                lon: 41.038078,
                lat: 30.975309
            }
        },
        {
            nameEn: "Banbān",
            name: "بنبان",
            id: 107684,
            country: "SA",
            coord: {
                lon: 46.588058,
                lat: 25.001671
            }
        },
        {
            nameEn: "Rafḩā",
            name: "رفح",
            id: 103012,
            country: "SA",
            coord: {
                lon: 43.519409,
                lat: 29.634251
            }
        },
        {
            nameEn: "Al Qaysumah",
            name: "القيصومة",
            id: 108918,
            country: "SA",
            coord: {
                lon: 46.127289,
                lat: 28.311171
            }
        },
        {
            nameEn: "Al Mikhlāf",
            name: "المخلف",
            id: 109122,
            country: "SA",
            coord: {
                lon: 44.213501,
                lat: 17.53957
            }
        },
        {
            nameEn: "Sīdī Ḩamzah",
            name: "سيدي حمزه",
            id: 101927,
            country: "SA",
            coord: {
                lon: 39.615921,
                lat: 24.506149
            }
        },
        {
            nameEn: "Birzayn",
            name: "بيرزاين",
            id: 107392,
            country: "SA",
            coord: {
                lon: 42.783329,
                lat: 18.25
            }
        },
        {
            nameEn: "Masjid Ibn Rashīd",
            name: "مسجد بن رشيد",
            id: 104270,
            country: "SA",
            coord: {
                lon: 45.400002,
                lat: 27.91667
            }
        },
        {
            nameEn: "Sidīs",
            name: "سيديس",
            id: 395014,
            country: "SA",
            coord: {
                lon: 41.674049,
                lat: 27.470261
            }
        },
        {
            nameEn: "Minţaqat al Jawf",
            name: "منطقة الجوف",
            id: 109470,
            country: "SA",
            coord: {
                lon: 38.75,
                lat: 29.5
            }
        },
        {
            nameEn: "Al Qurayyat",
            name: "Al Qurayyat",
            id: 108648,
            country: "SA",
            coord: {
                lon: 37.342819,
                lat: 31.33176
            }
        },
        {
            nameEn: "Al Başar",
            name: "البسار",
            id: 109923,
            country: "SA",
            coord: {
                lon: 43.869732,
                lat: 26.285299
            }
        },
        {
            nameEn: "Ad Dawadimi",
            name: "الدوادمي",
            id: 110325,
            country: "SA",
            coord: {
                lon: 44.392368,
                lat: 24.507721
            }
        },
        {
            nameEn: "Sufan",
            name: "سفيان",
            id: 101810,
            country: "SA",
            coord: {
                lon: 42.599998,
                lat: 19.98333
            }
        },
        {
            nameEn: "Al ‘Aqīq",
            name: "العقيق",
            id: 110060,
            country: "SA",
            coord: {
                lon: 41.664341,
                lat: 20.273581
            }
        },
        {
            nameEn: "Ar Ruqayyiqah",
            name: "الرقية",
            id: 108384,
            country: "SA",
            coord: {
                lon: 49.566669,
                lat: 25.35
            }
        },
        {
            nameEn: "Ḩajlah",
            name: "حج الله",
            id: 106261,
            country: "SA",
            coord: {
                lon: 42.633331,
                lat: 18.25
            }
        },
        {
            nameEn: "Khishaybī al Ḩaḑar",
            name: "خشيبي الحضر",
            id: 396460,
            country: "SA",
            coord: {
                lon: 43.283531,
                lat: 25.762119
            }
        },
        {
            nameEn: "Tarut",
            name: "تاروت",
            id: 101554,
            country: "SA",
            coord: {
                lon: 50.036942,
                lat: 26.57332
            }
        },
        {
            nameEn: "Sultanah",
            name: "السلطانة",
            id: 101760,
            country: "SA",
            coord: {
                lon: 39.58572,
                lat: 24.49258
            }
        },
        {
            nameEn: "Sayhat",
            name: "سيهات",
            id: 102318,
            country: "SA",
            coord: {
                lon: 50.040459,
                lat: 26.48522
            }
        },
        {
            nameEn: "Sakaka",
            name: "سكاكا",
            id: 102527,
            country: "SA",
            coord: {
                lon: 40.206409,
                lat: 29.96974
            }
        },
        {
            nameEn: "Safwa",
            name: "صقوي",
            id: 102585,
            country: "SA",
            coord: {
                lon: 49.952229,
                lat: 26.64986
            }
        },
        {
            nameEn: "Rahimah",
            name: "رحيمة",
            id: 102985,
            country: "SA",
            coord: {
                lon: 50.061939,
                lat: 26.707911
            }
        },
        {
            nameEn: "Rabigh",
            name: "برابغ",
            id: 103035,
            country: "SA",
            coord: {
                lon: 39.034931,
                lat: 22.798559
            }
        },
        {
            nameEn: "Qalat Bishah",
            name: "قلعة بيشة",
            id: 103369,
            country: "SA",
            coord: {
                lon: 42.605202,
                lat: 20.00054
            }
        },
        {
            nameEn: "Najran",
            name: "نجران",
            id: 103630,
            country: "SA",
            coord: {
                lon: 44.127659,
                lat: 17.492399
            }
        },
        {
            nameEn: "Zbe",
            name: "ظبي",
            id: 106909,
            country: "SA",
            coord: {
                lon: 35.69014,
                lat: 27.351339
            }
        },
        {
            nameEn: "Badr Hunayn",
            name: "بدر حنين",
            id: 107744,
            country: "SA",
            coord: {
                lon: 38.79047,
                lat: 23.782921
            }
        },
        {
            nameEn: "At Taraf",
            name: "الطرف",
            id: 107959,
            country: "SA",
            coord: {
                lon: 49.72504,
                lat: 25.362761
            }
        },
        {
            nameEn: "As Sulayyil",
            name: "السليل",
            id: 108048,
            country: "SA",
            coord: {
                lon: 45.577919,
                lat: 20.46067
            }
        },
        {
            nameEn: "Al Khubar",
            name: "الخبر",
            id: 109323,
            country: "SA",
            coord: {
                lon: 50.208328,
                lat: 26.27944
            }
        },
        {
            nameEn: "Al Khafji",
            name: "الخفجي",
            id: 109380,
            country: "SA",
            coord: {
                lon: 48.491322,
                lat: 28.439051
            }
        },
        {
            nameEn: "Al Jumum",
            name: "الجموم",
            id: 109417,
            country: "SA",
            coord: {
                lon: 39.698059,
                lat: 21.61694
            }
        },
        {
            nameEn: "Al Hufuf",
            name: "الهفوف",
            id: 109571,
            country: "SA",
            coord: {
                lon: 49.565319,
                lat: 25.364571
            }
        },
        {
            nameEn: "Al Bukayriyah",
            name: "البكيرية",
            id: 109878,
            country: "SA",
            coord: {
                lon: 43.659328,
                lat: 26.14422
            }
        },
        {
            nameEn: "Al Battaliyah",
            name: "البطالية",
            id: 109915,
            country: "SA",
            coord: {
                lon: 49.633331,
                lat: 25.433331
            }
        },
        {
            nameEn: "Al Bahah",
            name: "الباحة",
            id: 109953,
            country: "SA",
            coord: {
                lon: 41.46767,
                lat: 20.01288
            }
        },
        {
            nameEn: "Abu Arish",
            name: "ابو العريش",
            id: 110619,
            country: "SA",
            coord: {
                lon: 42.832512,
                lat: 16.96887
            }
        },
        {
            nameEn: "Al Qudayḩ",
            name: "القضي",
            id: 414980,
            country: "SA",
            coord: {
                lon: 49.989441,
                lat: 26.56889
            }
        },
        {
            nameEn: "Umm as Sahik",
            name: "أم ساهيك",
            id: 101035,
            country: "SA",
            coord: {
                lon: 49.916389,
                lat: 26.65361
            }
        },
        {
            nameEn: "At Tubi",
            name: "التوبي",
            id: 101344,
            country: "SA",
            coord: {
                lon: 49.991669,
                lat: 26.557779
            }
        },
        {
            nameEn: "Tanumah",
            name: "تنومة",
            id: 101581,
            country: "SA",
            coord: {
                lon: 44.133331,
                lat: 27.1
            }
        },
        {
            nameEn: "Tabalah",
            name: "تبالة",
            id: 101633,
            country: "SA",
            coord: {
                lon: 42.400002,
                lat: 19.950001
            }
        },
        {
            nameEn: "Mulayjah",
            name: "مليجة",
            id: 103922,
            country: "SA",
            coord: {
                lon: 48.424191,
                lat: 27.27103
            }
        },
        {
            nameEn: "Misliyah",
            name: "المصلية",
            id: 104269,
            country: "SA",
            coord: {
                lon: 42.557201,
                lat: 17.459881
            }
        },
        {
            nameEn: "Marat",
            name: "مارات",
            id: 104376,
            country: "SA",
            coord: {
                lon: 45.461472,
                lat: 25.07007
            }
        },
        {
            nameEn: "Julayjilah",
            name: "جليجلة",
            id: 105252,
            country: "SA",
            coord: {
                lon: 49.599998,
                lat: 25.5
            }
        },
        {
            nameEn: "Farasan",
            name: "الفرسان",
            id: 106744,
            country: "SA",
            coord: {
                lon: 42.118328,
                lat: 16.702221
            }
        },
        {
            nameEn: "Sajir",
            name: "ساجر",
            id: 108121,
            country: "SA",
            coord: {
                lon: 44.59964,
                lat: 25.18251
            }
        },
        {
            nameEn: "As Saffaniyah",
            name: "الصفانية",
            id: 108142,
            country: "SA",
            coord: {
                lon: 48.73,
                lat: 27.970831
            }
        },
        {
            nameEn: "Al Qurayn",
            name: "القرين",
            id: 108890,
            country: "SA",
            coord: {
                lon: 49.599998,
                lat: 25.48333
            }
        },
        {
            nameEn: "Al Qarah",
            name: "القارة",
            id: 108957,
            country: "SA",
            coord: {
                lon: 49.666672,
                lat: 25.41667
            }
        },
        {
            nameEn: "Al Mutayrifi",
            name: "المطيرفي",
            id: 109008,
            country: "SA",
            coord: {
                lon: 49.557629,
                lat: 25.47879
            }
        },
        {
            nameEn: "Al Mindak",
            name: "المندق",
            id: 109118,
            country: "SA",
            coord: {
                lon: 41.283371,
                lat: 20.1588
            }
        },
        {
            nameEn: "Al Markaz",
            name: "المركز",
            id: 109165,
            country: "SA",
            coord: {
                lon: 49.73333,
                lat: 25.4
            }
        },
        {
            nameEn: "Al Jubayl",
            name: "الجبيل",
            id: 109436,
            country: "SA",
            coord: {
                lon: 49.650002,
                lat: 25.4
            }
        },
        {
            nameEn: "Al Jaradiyah",
            name: "الجرادية",
            id: 109481,
            country: "SA",
            coord: {
                lon: 42.912399,
                lat: 16.57946
            }
        },
        {
            nameEn: "Al Jafr",
            name: "الجفر",
            id: 109502,
            country: "SA",
            coord: {
                lon: 49.72361,
                lat: 25.37694
            }
        },
        {
            nameEn: "Al Artawiyah",
            name: "الارطاوية",
            id: 110031,
            country: "SA",
            coord: {
                lon: 45.344372,
                lat: 26.50526
            }
        },
        {
            nameEn: "Ad Darb",
            name: "الدرب",
            id: 110328,
            country: "SA",
            coord: {
                lon: 42.252609,
                lat: 17.722851
            }
        },
        {
            nameEn: "Hajrat al Khinbish",
            name: "هجرة الخنبش",
            id: 391699,
            country: "SA",
            coord: {
                lon: 45.036751,
                lat: 27.78248
            }
        },
        {
            nameEn: "Suwayr",
            name: "سوير",
            id: 392753,
            country: "SA",
            coord: {
                lon: 40.389252,
                lat: 30.11713
            }
        },
        {
            nameEn: "Tumayr",
            name: "تمير",
            id: 396550,
            country: "SA",
            coord: {
                lon: 45.867962,
                lat: 25.70393
            }
        },
        {
            nameEn: "Al Fuwayliq",
            name: "الفويلق",
            id: 397513,
            country: "SA",
            coord: {
                lon: 43.253399,
                lat: 26.44322
            }
        },
        {
            nameEn: "Al Majaridah",
            name: "المجاردة",
            id: 399518,
            country: "SA",
            coord: {
                lon: 41.91111,
                lat: 19.12361
            }
        },
        {
            nameEn: "Al Muwayh",
            name: "المويه",
            id: 409993,
            country: "SA",
            coord: {
                lon: 41.758289,
                lat: 22.433331
            }
        },
        {
            nameEn: "Al Hada",
            name: "الهدا",
            id: 410084,
            country: "SA",
            coord: {
                lon: 40.286942,
                lat: 21.367531
            }
        },
        {
            nameEn: "Mizhirah",
            name: "مزهرة",
            id: 410874,
            country: "SA",
            coord: {
                lon: 42.73333,
                lat: 16.826111
            }
        },
        {
            nameEn: "shokhaib",
            name: "شكيب",
            id: 6692745,
            country: "SA",
            coord: {
                lon: 46.268711,
                lat: 24.490231
            }
        },
        {
            nameEn: "Al Ghaţghaţ",
            name: "الغاجه",
            id: 109771,
            country: "SA",
            coord: {
                lon: 46.249908,
                lat: 24.47649
            }
        },
        {
            nameEn: "Al Khālidīyah",
            name: "الخالدية",
            id: 448600,
            country: "SA",
            coord: {
                lon: 46.759441,
                lat: 24.622219
            }
        }
    ]