import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

// import { Http } from '@angular/http';
import { CITIES } from '../../providers/sa-cities';
import { SelectSearchableComponent } from 'ionic-select-searchable';
import { Geolocation } from '@ionic-native/geolocation';
import { HTTP } from '@ionic-native/http';

@IonicPage()
@Component({
  selector: 'page-weather',
  templateUrl: 'weather.html',
})
export class WeatherPage {

  days = ['الاحد', 'الاثنين', 'الثلاثاء', 'الاربعاء', 'الخميس', 'الجمعه', 'السبت'];

  test: string;
  url7days: string;
  Lastupdate: any;

  Min_TempC: any;
  Max_tempC: any;
  IMg: any = ''

  Is_DayString: any;
  HeavyRain: any;
  SunnyString: any;
  ArrOptions: any;
  SelectedItem: any;

  FirstDay_Max: any;
  FirstDay_Min: any;


  ArrContain7Days: any = [];
  Arr_contain_Temp_Of_7Days: any = [];
  LOAD: boolean = false;

  constructor(public navCtrl: NavController,
    private geolocation: Geolocation,
    public navParams: NavParams, private http: HTTP) {
    this.ArrOptions = CITIES;
    if (this.ArrOptions[0].id != 0) {
      this.ArrOptions.unshift({
        nameEn: "Current",
        name: "مكاني الحالي",
        id: 0,
      });
    }

    this.SelectedItem = this.ArrOptions[0];
    this.setCurrentLocation();
    // this.optionsFn()
  }

  setCurrentLocation() {
    this.geolocation.getCurrentPosition().then((resp) => {
      let currentLat = resp.coords.latitude;
      let currentLong = resp.coords.longitude;
      let url: string = 'https://api.apixu.com/v1/forecast.json?key=230f30cb487d465eb0593034182003&lang=ar&q=' + currentLat + ',' + currentLong + '&days=7'
      this.optionsFn(url)
    }).catch((error) => {
      this.SelectedItem = this.ArrOptions[21];
      this.optionsFn();
      console.log('Error getting location', error);
    });
  }

  ionViewDidLoad() {
  }

  ChnageSelectItemFromSelect(event: { component: SelectSearchableComponent, value: any }) {
    debugger
    if (event.value.id == 0) {
      this.setCurrentLocation()
    } else {
      this.optionsFn()
    }
  }

  optionsFn(url = null) {
    var Item = this.SelectedItem.nameEn;
    this.Arr_contain_Temp_Of_7Days = [];
    this.ArrContain7Days = [];

    this.test = url || 'https://api.apixu.com/v1/forecast.json?key=230f30cb487d465eb0593034182003&lang=ar&q=' + Item + '&days=7'
    this.LOAD = false;
    this.http.get(this.test , {} ,{}).then(da => {

      let res = JSON.parse(da.data);
      for (let index = 1; index < res.forecast.forecastday.length; index++) {
        this.ArrContain7Days.push(this.days[new Date(res.forecast.forecastday[index].date).getDay()])
        this.Arr_contain_Temp_Of_7Days.push(res.forecast.forecastday[index])
      }
      this.Max_tempC = res.forecast.forecastday[0].day.maxtemp_c
      this.Min_TempC = res.forecast.forecastday[0].day.mintemp_c
      this.IMg = 'https:' + res.current.condition.icon
      this.HeavyRain = res.current.precip_mm;
      this.SunnyString = res.current.condition.text;
      this.Lastupdate = res.current.last_updated;

      if (res.current.is_day == 1) {
        this.Is_DayString = 'نهار'
      } else {
        this.Is_DayString = 'الليل'
      }
      this.LOAD = true;
    })
  }

  Back() {
    this.navCtrl.pop()
  }

  GotoSettingPage() {
    this.navCtrl.push('SettingPage')
  }
  gotaboutpage() {
    this.navCtrl.push('AboutUsPage');
  }

}
