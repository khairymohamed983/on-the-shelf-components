import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, Platform } from 'ionic-angular';
import * as PDFJS from "pdfjs-dist/webpack.js";
import { PDFPageProxy, PDFPageViewport, PDFRenderTask } from 'pdfjs-dist';

@IonicPage()
@Component({
  selector: 'page-openpdf',
  templateUrl: 'openpdf.html',
})

export class OpenpdfPage {
  title: string = '';
  url: string = '';
  pageNumer: number = 1;
  pdfDocument: PDFJS.PDFDocumentProxy;
  PDFJSViewer = PDFJS;

  width: number = 0;
  height: number = 0;
  zoom: number = 0;

  pageContainerUnique = {
    width: 0 as number,
    height: 0 as number,
    element: null as HTMLElement,
    canvas: null as HTMLCanvasElement,
    textContainer: null as HTMLElement,
    canvasWrapper: null as HTMLElement
  }

  @ViewChild('pageContainer') pageContainerRef: ElementRef;
  @ViewChild('viewer') viewerRef: ElementRef;
  @ViewChild('canvas') canvasRef: ElementRef;
  @ViewChild('canvasWrapper') canvasWrapperRef: ElementRef;
  @ViewChild('textContainer') textContainerRef: ElementRef;

  pdfPage: PDFPageProxy;
  loaded: boolean = false;

  constructor(public navCtrl: NavController,
    private platform: Platform,
    public navParams: NavParams, public events: Events) {
    this.title = this.navParams.get('title');
    this.url = this.navParams.get("file");

    platform.ready().then((readySource) => {
      this.width = platform.width() - 5;
      this.height = platform.height();
    });  
  }

  zoomIn() {
    this.zoom += 0.15;
    this.renderOnePage();
  }

  zoomOut() {
    this.zoom = this.zoom == 0.5 ? 0.5 : this.zoom - 0.15;
    this.renderOnePage();
  }

  ionViewDidLoad() {
    // this.pageContainerUnique.element = this.pageContainerRef.nativeElement as HTMLElement;
    // this.pageContainerUnique.canvasWrapper = this.canvasWrapperRef.nativeElement as HTMLCanvasElement;
    // this.pageContainerUnique.canvas = this.canvasRef.nativeElement as HTMLCanvasElement;
    // this.pageContainerUnique.textContainer = this.textContainerRef.nativeElement as HTMLCanvasElement;
    // this.loadPdf(this.url);
    //http://www.qssna.org/Books/ar_Fortress_of_the_Muslim_Explanation.pdf,http://www.usislam.org/pdf/hisn-almuslim-Arabic.pdf'
  }

  loadProgress :number = 0 ;

  progressCallback(progress) {
    this.loadProgress = (progress.loaded/progress.total)*100;
  }

  error = false;
  loadPdf(pdfPath: string) {
    try {
      this.PDFJSViewer.disableStream  = true;
      this.PDFJSViewer.disableAutoFetch  = true;
  
      this.PDFJSViewer.getDocument(pdfPath, null, null, this.progressCallback)
        .then(pdf => {
          this.loaded = true;
          this.pdfDocument = pdf;
          return this.loadPage(1);
        }).then((pdfPage) => {
          console.dir(pdfPage);
        }).catch(e => {
          this.loaded = true;
          this.error = true;
          console.error(e);
          return false;
        });
    } catch (error) {
      this.loaded = true;
      this.error = true;
    }
  }

  loadPage(pageNum: number = 1) {
    let pdfPage: PDFPageProxy;
    this.pdfDocument.getPage(pageNum).then(thisPage => {
      pdfPage = thisPage;
      this.pdfPage = pdfPage;
      this.getReversedScaleFromScreanSize();
      this.renderOnePage();
    });
  }

  getReversedScaleFromScreanSize() {
    let viewport = this.pdfPage.getViewport(1) as PDFPageViewport;
    this.zoom = this.zoom || this.width / viewport.width;
  }

  async renderOnePage() {

    let textContainer: HTMLElement;
    let canvas: HTMLCanvasElement;
    let wrapper: HTMLElement;

    let canvasContext: CanvasRenderingContext2D;
    let page: HTMLElement

    page = this.pageContainerUnique.element;
    textContainer = this.pageContainerUnique.textContainer;
    canvas = this.pageContainerUnique.canvas;
    wrapper = this.pageContainerUnique.canvasWrapper;

    canvasContext = canvas.getContext('2d') as CanvasRenderingContext2D;
    canvasContext.imageSmoothingEnabled = false;
    canvasContext.webkitImageSmoothingEnabled = false;
    canvasContext.mozImageSmoothingEnabled = false;
    canvasContext.oImageSmoothingEnabled = false;

    // this is get the original pdf page width and height we need the screan size 
    let viewport = this.pdfPage.getViewport(this.zoom) as PDFPageViewport;
    // viewport.width = this.width;
    // viewport.height = this.height;

    canvas.width = viewport.width;
    canvas.height = viewport.height;
    page.style.width = `${viewport.width}px`;
    page.style.height = `${viewport.height}px`;
    wrapper.style.width = `${viewport.width}px`;
    wrapper.style.height = `${viewport.height}px`;
    textContainer.style.width = `${viewport.width}px`;
    textContainer.style.height = `${viewport.height}px`;
    //fix for 4K
    if (window.devicePixelRatio > 1) {
      let canvasWidth = canvas.width;
      let canvasHeight = canvas.height;
      canvas.width = canvasWidth * window.devicePixelRatio;
      canvas.height = canvasHeight * window.devicePixelRatio;
      canvas.style.width = canvasWidth + "px";
      canvas.style.height = canvasHeight + "px";
      canvasContext.scale(window.devicePixelRatio, window.devicePixelRatio);
    }

    let renderTask: PDFRenderTask = this.pdfPage.render({
      canvasContext,
      viewport
    });

    let container = textContainer;
    return renderTask.then(() => {
      return this.pdfPage.getTextContent();
    }).then((textContent) => {
      let textLayer: HTMLElement;
      textLayer = this.pageContainerUnique.textContainer
      while (textLayer.lastChild) {
        textLayer.removeChild(textLayer.lastChild);
      }
      this.PDFJSViewer.renderTextLayer({
        textContent,
        container,
        viewport,
        textDivs: []
      });

      return true;
    });
  }
  prevPage() {
    if (this.pageNumer <= 1) {
      return;
    }
    this.pageNumer--;
    this.loadPage(this.pageNumer);
  }
  nextPage() {
    if (this.pageNumer >= this.pdfDocument.numPages) {
      return;
    }
    this.pageNumer++;
    this.loadPage(this.pageNumer);
  }
  Back() {
    this.navCtrl.pop()
  }
}
