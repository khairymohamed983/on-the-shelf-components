import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { HTTP } from '@ionic-native/http';
import { Geolocation } from '@ionic-native/geolocation';
import * as moment from 'moment';

import { CITIES } from '../../providers/sa-cities';
import { SelectSearchableComponent } from 'ionic-select-searchable';
@IonicPage()

@Component({ selector: 'page-pray', templateUrl: 'pray.html' })

export class PrayPage {
  date = new Date();
  ThisDay: any;
  ThisMonth: any;
  ThisYear: any;
  Check_Pm_Or_Am_Fajr: any;
  Check_Pm_Or_Am_Duhr: any;
  Check_Pm_Or_Am_Asr: any;
  Check_Pm_Or_Am_Maghrib: any;
  Check_Pm_Or_Am_Isha: any;
  Come_prey: any;
  Hours: any;
  Minutes: any;

  ItemSeleted: any;

  LOAD: boolean = false;
  AUDIO: any;

  lstCITIES: any[] = [];

  MonthsOfYear: any = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December'
  ]

  PreyerTimings: any = {
    Fajr: '',
    Duhr: '',
    Asr: '',
    Maghrib: '',
    Isha: ''
  }

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private http: HTTP, 
    private geolocation: Geolocation, private alert: AlertController
  ) {
    // this.AUDIO = new Audio('assets/azan.mp3');
    this.lstCITIES = CITIES;
    if (this.lstCITIES[0].id != 0) {
      this.lstCITIES.unshift({
        nameEn: "Current",
        name: "مكاني الحالي",
        id: 0,
      });
    }

    this.ItemSeleted = this.lstCITIES[0];
    this.setCurrentLocation();
  }

  setCurrentLocation() {
    this.geolocation.getCurrentPosition().then((resp) => {
      console.log(resp);
      let currentLat = resp.coords.latitude;
      let currentLong = resp.coords.longitude;
      let date = new Date();
      let url: string = "http://api.aladhan.com/v1/calendar?latitude=" + currentLat + "&longitude=" + currentLong + "&method=3&month=" + (date.getMonth() + 1) + "&year=" + date.getFullYear();
      this.GetDataFromApi(url)
    }).catch((error) => {
      this.ItemSeleted = this.lstCITIES[21];
      let url: string = 'http://api.aladhan.com/v1/calendarByCity?city=' + this.ItemSeleted.nameEn + '&country=saudi arabia&method=3';
      this.GetDataFromApi(url)
      console.log('Error getting location', error);
    });
  }

  precisionRound(number, precision) {
    var factor = Math.pow(10, precision);
    return Math.round(number * factor) / factor;
  }

  gotoCompass() {
    this
      .navCtrl
      .push('CompassPage');
  }

  ionViewDidLoad() {
    // this
    //   .localnotification
    //   .schedule({
    //     id: 1,
    //     title: 'تنبيه الآذان',
    //     text: 'تنبيه آذان المغرب',
    //     // trigger: new Date(),
    //     sound: 'file://assets/azan.mp3'
    //   });
  }
 
  Back() {
    this
      .navCtrl
      .pop()
  }

  GotoSettingPage() {
    this
      .navCtrl
      .push('SettingPage')
  }

  gotaboutpage() {
    this
      .navCtrl
      .push('AboutUsPage');
  }

  ChnageSelectItemFromSelect(event: { component: SelectSearchableComponent, value: any }) {
    console.log('port:', event.value);
    if (event.value.id == 0) {
      this.setCurrentLocation()
    } else {
      let Innerurl = 'http://api.aladhan.com/v1/calendarByCity?city=' + this.ItemSeleted.nameEn + '&country=saudi arabia&method=3'
      this.GetDataFromApi(Innerurl)
    }
  }




  morningandEviningString(time) {
    if (time.split(' ')[0].split(':')[0] <= 12 && time.split(' ')[0].split(':')[0] < 6 && time.split(' ')[0].split(':')[1] < 60) {
      return 'ص'
    } else {
      return 'م'
    }
  }

  GetDataFromApi(uri) {
    let dnow = new Date()
    this.LOAD = false;
    this
      .http
      .get(uri , {} , {}) 
      .then(res => {
        console.log(res);

        this.LOAD = true;

        let today = new Date();
        let dd = today.getDate();
        let mm = today.getMonth() + 1;
        let month: string;
        var yyyy = today.getFullYear();
        let day = dd.toString();
        if (dd < 10) {
          day = '0' + dd;
        }
        if (mm < 10) {
          month = '0' + mm;
        }
        var current = day + '-' + month + '-' + yyyy;
        let d = JSON.parse(res.data)

        let data = d.data;
        for (let index = 0; index < data.length; index++) {

          if (data[index].date.gregorian.date == current) {
            this.Check_Pm_Or_Am_Fajr = this.morningandEviningString(data[index].timings.Fajr);
            this.Check_Pm_Or_Am_Duhr = this.morningandEviningString(data[index].timings.Dhuhr);
            this.Check_Pm_Or_Am_Asr = this.morningandEviningString(data[index].timings.Asr);
            this.Check_Pm_Or_Am_Maghrib = this.morningandEviningString(data[index].timings.Maghrib);
            this.Check_Pm_Or_Am_Isha = this.morningandEviningString(data[index].timings.Isha);

            this.PreyerTimings.Fajr = data[index].timings.Fajr.split(' ')[0];
            this.PreyerTimings.Duhr = data[index].timings.Dhuhr.split(' ')[0]
            this.PreyerTimings.Asr = data[index].timings.Asr.split(' ')[0]
            this.PreyerTimings.Maghrib = data[index].timings.Maghrib.split(' ')[0]
            this.PreyerTimings.Isha = data[index].timings.Isha.split(' ')[0]
            console.log(this.PreyerTimings);

            let milsecondofnow = dnow.getTime()
            let TimeOfNow = moment(milsecondofnow).format('hh:mm a')

            let pm_am = TimeOfNow.split(' ')[1]

            if (pm_am === 'am') {

              if (TimeOfNow === data[index].timings.Fajr.split(' ')[0] || TimeOfNow === data[index].timings.Dhuhr.split(' ')[0]) {

                // this
                //   .background
                //   .enable()
                // if (this.background.isEnabled()) {
                //   this
                //     .alert
                //     .create({})
                //     .present()
                //   this
                //     .background
                //     .on('activate')
                //     .subscribe(res => {
                //       this
                //         .AUDIO
                //         .play()
                //     })
                // }
              }
              if (TimeOfNow >= '12' && TimeOfNow <= '1') {
                this.Come_prey = 'الفجر'
                this.Calculate_Remaining_Time(TimeOfNow, data[index].timings.Fajr.split(' ')[0])
              } else if (TimeOfNow <= data[index].timings.Fajr.split(' ')[0]) {
                this.Come_prey = 'الفجر'

                this.Calculate_Remaining_Time(TimeOfNow, data[index].timings.Fajr.split(' ')[0])
              } else if (TimeOfNow > data[index].timings.Fajr.split(' ')[0] && TimeOfNow <= data[index].timings.Dhuhr.split(' ')[0]) {
                this.Come_prey = 'الضهر'

                this.Calculate_Remaining_Time(TimeOfNow, data[index].timings.Dhuhr.split(' ')[0])
              }

            } else if (pm_am === 'pm') {
              let TimeNow24H = moment(milsecondofnow).format('HH:mm ')

              if (TimeOfNow === data[index].timings.Dhuhr.split(' ')[0] || TimeNow24H === data[index].timings.Asr.split(' ')[0] || TimeNow24H === data[index].timings.Maghrib.split(' ')[0] || TimeNow24H === data[index].timings.Isha.split(' ')[0]) {
                this
                  .alert
                  .create({})
                  .present()
                // this
                //   .background
                //   .enable()
                // if (this.background.isEnabled()) {
                //   this
                //     .background
                //     .on('activate')
                //     .subscribe(res => {
                //       this
                //         .AUDIO
                //         .play()
                //     })
                // }
              }

              if (TimeNow24H <= data[index].timings.Dhuhr.split(' ')[0]) {
                this.Come_prey = 'الضهر'

                this.Calculate_Remaining_Time(TimeNow24H, data[index].timings.Dhuhr.split(' ')[0])
              } else if (TimeNow24H > data[index].timings.Dhuhr.split(' ')[0] && TimeNow24H <= data[index].timings.Asr.split(' ')[0]) {
                this.Come_prey = 'العصر'

                this.Calculate_Remaining_Time(TimeNow24H, data[index].timings.Asr.split(' ')[0])
              } else if (TimeNow24H > data[index].timings.Asr.split(' ')[0] && TimeNow24H <= data[index].timings.Maghrib.split(' ')[0]) {
                this.Come_prey = 'المغرب'

                this.Calculate_Remaining_Time(TimeNow24H, data[index].timings.Maghrib.split(' ')[0])
              } else if (TimeNow24H > data[index].timings.Maghrib.split(' ')[0] && TimeNow24H <= data[index].timings.Isha.split(' ')[0]) {
                this.Come_prey = 'العشاء'

                this.Calculate_Remaining_Time(TimeNow24H, data[index].timings.Isha.split(' ')[0])
              } else if (TimeNow24H > data[index].timings.Isha.split(' ')[0] && pm_am === 'pm') {
                this.Come_prey = 'الفجر'

                let first_parttime = 12 - + TimeOfNow.split(':')[0] - 1
                let second_parttime = 60 - + TimeOfNow
                  .split(':')[1]
                  .split(' ')[0]

                let first_parttime2 = +res
                  .data[index]
                  .timings
                  .Fajr
                  .split(' ')[0]
                  .split(':')[0] - 0
                let second_parttime2 = +res
                  .data[index]
                  .timings
                  .Fajr
                  .split(' ')[0]
                  .split(':')[1] - 0

                if (second_parttime + second_parttime2 >= 60) {
                  this.Minutes = second_parttime + second_parttime2 - 60
                  this.Hours = first_parttime + first_parttime2 + 1
                } else if (second_parttime + second_parttime2 == 120) {
                  this.Minutes = 0
                  this.Hours = first_parttime + first_parttime2 + 2
                } else {
                  this.Minutes = second_parttime + second_parttime2
                  this.Hours = first_parttime + first_parttime2
                }

              }
            }
            console.log(this.PreyerTimings);

            break;
          } //end Bigif
        } //end for 
      })
  } // end Come From Api

  Calculate_Remaining_Time(TimeNow, TimePray) {
    let starttime = moment(TimeNow, 'hh:mm a')
    let endtime = moment(TimePray, 'hh:mm a')
    if (starttime > endtime) {
      let duration = moment.duration(starttime.diff(endtime))
      this.Hours = Math.floor(duration.asHours())
      this.Minutes = Math.floor(duration.asMinutes() - this.Hours * 60)
    } else if (endtime > starttime) {
      let duration = moment.duration(endtime.diff(starttime))
      this.Hours = Math.floor(duration.asHours())
      this.Minutes = Math.floor(duration.asMinutes() - this.Hours * 60)
    } else {
      let duration = moment.duration(endtime.diff(starttime))
      this.Hours = Math.floor(duration.asHours())
      this.Minutes = Math.floor(duration.asMinutes() - this.Hours * 60)
    }
  }
}
