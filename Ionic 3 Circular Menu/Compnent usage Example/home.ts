import { Component, ViewChild } from '@angular/core';
import { NavController, IonicPage } from 'ionic-angular';
import { MenuItem } from '../../components/circular-tabs/menu-Item'
import { CircularTabs } from '../../components/circular-tabs/circular-tabs';
@IonicPage()

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage {
 
  menuItems: MenuItem[] = [
    {
      title: 'القرأن و الاذاعه',
      img: 'assets/imgs/icon-1.png',
      page: 'PrequraanPage',
      isSelected: false
    },
    {
      title: 'مكتبه الجهاز',
      img: 'assets/imgs/icon-2.png',
      page: 'LiberaryPage',
      isSelected: false
    },
    {
      title: 'معارض الجهاز',
      img: 'assets/imgs/icon-3.png',
      page: 'GallaryPage',
      isSelected: false
    },
    {
      title: 'مجله الارشاد',
      img: 'assets/imgs/icon-4.png',
      page: 'MagazineGuidePage',
      isSelected: false
    },
    {
      title: undefined,
      img: 'assets/imgs/onerow.png',
      page: 'SafWa7edPage',
      isSelected: false
    },
    {
      title: 'التقويم',
      img: 'assets/imgs/icon-5.png',
      page: 'CalenderPage',
      isSelected: false
    },
    {
      title: 'احوال الطقس',
      img: 'assets/imgs/icon-6.png',
      page: 'WeatherPage',
      isSelected: false
    },
    {
      title: 'الصلاه والاذكار',
      img: 'assets/imgs/icon-7.png',
      page: 'PreprayPage',
      isSelected: false
    }
  ]

 
  @ViewChild(CircularTabs)
  private menu: CircularTabs;

  public theBoundCallback: Function;

  constructor(public navCtrl: NavController) {
    this.theBoundCallback = this.goToPage.bind(this);

  }

  public ngOnInit() {
  }
  
  onDone($event, im) {
    console.log(im);
    // this.seedLocation(im.toString())
  }

  goToPage(page) { 
    console.log('Ihave Bee triggered now going to navigate to the page');
    this
      .navCtrl
      .push(page);
  }

  gotaboutpage() {
    this
      .navCtrl
      .push('AboutUsPage');
  }
  GotoSettingPage() {
    this.navCtrl.push('SettingPage')
  }
  ionViewWillEnter() {
    // alert('back again')
    console.log('Hiiiiiiiiiiiii******');
    this.menu.isNavOpened = true;
    this.menu.styleMenu();
    // this.isOpened = true;
  }
}
