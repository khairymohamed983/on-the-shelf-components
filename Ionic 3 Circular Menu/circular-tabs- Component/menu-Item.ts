export interface MenuItem {
    title : string;
    img :string;
    page:string;
    isSelected : boolean;
}