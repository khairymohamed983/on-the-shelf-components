import {
  Component,
  ElementRef,
  Renderer,
  ViewChild,
  Input,
  OnChanges,
  SimpleChanges,
  AfterViewInit,
  trigger,
  state,
  style,
  transition,
  animate
} from '@angular/core';

 import { MenuItem } from './menu-Item';
 
@Component({
  selector: 'circular-tabs',
  templateUrl: 'circular-tabs.html',
  host: {
    '(document:click)': 'onClick($event)'
  },
  animations: [trigger('menu', [
    state('open', style({ transform: 'scale(1) rotate(-20deg)', opacity: '1' })),
    state('close', style({ transform: 'scale(0.3)', opacity: '0' })),
    transition('close => open', animate('300ms ease-in')),
    transition('open => close', animate('300ms ease-out'))
  ])]
})

export class CircularTabs implements OnChanges,
  AfterViewInit {
  @ViewChild('list') listRef: ElementRef;
  @ViewChild('menuButton') menuButtonRef: ElementRef;
  @Input() public openPage: Function;
  @Input() menuItems: MenuItem[];
  @Input() totalAngle: number = 180;
  @Input() gapAngle: number = 2;
  @Input() startAngle: number = -10;
  @Input() closeOnTabSelection: boolean = true;
  @Input() closeOnBlur: boolean = true;
  @Input() closedBtnText: string = "";
  @Input() openedBtnText: string = "";
  @Input() closedBtnIconName: string = "";
  @Input() openedBtnIconName: string = "";

  // isNavOpened: boolean = false;
  isNavOpened: boolean = false;
  positionXLock: boolean = false;
  positionYLock: boolean = false;


  constructor(public elemRef: ElementRef, public renderer: Renderer) {
    setTimeout(() => {
      this.isNavOpened = true;
      // this.styleMenu();
    }, 750);
  }

  ngOnChanges(changes: SimpleChanges) {
    // if (changes['positionLock'].currentValue !=
    // changes['positionLock'].previousValue) { }
    this.styleMenu();

    this.logSettings();
  }

  transformationArray = [];
  ngAfterViewInit() {
    console.log('im here mother fu');

    // hide original tabbar
    setTimeout(() => {
      if (this.positionXLock) {
        this.lockElemPositionX();
      }
      if (this.positionYLock) {
        this.lockElemPositionY();
      }
      this.isNavOpened = true;

    }, 2000);

    this.styleMenu();
  }

  lockElemPositionY() {
    let top = window
      .getComputedStyle(this.elemRef.nativeElement, null)
      .getPropertyValue("top");
    this
      .renderer
      .setElementStyle(this.elemRef.nativeElement, 'top', top);
  }

  lockElemPositionX() {
    let left = window
      .getComputedStyle(this.elemRef.nativeElement, null)
      .getPropertyValue("left");
    this
      .renderer
      .setElementStyle(this.elemRef.nativeElement, 'left', left)
  }
 
  navigateToPage(index, page) {
    let full = this.getMyMovements(index)
    this.productMoveBackTransformationArray(full);
    let current = full.shift();
    full.pop();
    this.moveBack(current, full , [])
    this
      .renderer
      .setElementStyle(this.listRef.nativeElement.children[index].children[0], "background", "radial-gradient(transparent 35%, rgb(21, 27, 37) 36%)");
      setTimeout(() => { 
        this.isNavOpened = false;
        this.openPage(page);
      }, 2250);
      return;
  }

  productMoveBackTransformationArray(elements) {
    for (let i = 0; i < elements.length; i++) {
      const element = elements[i];
      let OldTransformation = this.transformationArray[element];
      if (element == 5 || element == 6 || element == 7) {
        this.setElementWithoutAnimation(element, OldTransformation);
        continue;
      } else {
        if (OldTransformation > 0) {
          this.transformationArray[element] = (OldTransformation - 360);
          this.setElementWithoutAnimation(element, (OldTransformation - 360));
        }
      }
    }
  }

  toggleNav() {
    this.isNavOpened
      ? this.closeNav()
      : this.openNav();
  }

  closeNav() {
    this.isNavOpened = false;
    console.log('navclosed');
  }

  openNav() {
    this.isNavOpened = true;
    this.styleMenu();
    console.log('navopened');
  }
  centeralAngleL = 0;

  styleMenu() {
    let numofTabs = this.menuItems.length;
    //this.tabRef._tabs.length;

    var centralAngel = (this.totalAngle / numofTabs) - (((numofTabs) * this.gapAngle) / numofTabs);
    let startAngle = this.startAngle >= 0
      ? this.startAngle
      : ((180 - this.totalAngle) / 2);
    this.centeralAngleL = centralAngel;
    // this dummy code added by khairy to customize this shit
    // const safaaTransformation = 'rotate(200.5deg) translateY(12px) translateX(11px);'
    let contentTransformation = 360;
    this.transformationArray = [];
    const contentTransformAmount = 45; // 360 / 8 - count of items  in our circle
    for (var i = 0; i < numofTabs; i++) {
      if (typeof (this.listRef.nativeElement.children[i]) != 'undefined' && this.listRef.nativeElement.children[i] != null) {
        //li
        this.transformationArray.push(((i * centralAngel) + startAngle + (i * this.gapAngle)));
        this
          .renderer
          .setElementStyle(this.listRef.nativeElement.children[i], "transform", "rotate(" + ((4 * centralAngel) + startAngle + (4 * this.gapAngle)) + "deg) skew(" + (90 - centralAngel) + "deg)");

        this
          .renderer
          .setElementStyle(this.listRef.nativeElement.children[i], "display", "none");
        this
          .renderer
          .setElementStyle(this.listRef.nativeElement.children[i].children[0], 'color', 'white');

        if (this.listRef.nativeElement.children[i].children[0].children[0].tagName == 'SPAN') {
          this
            .renderer
            .setElementStyle(this.listRef.nativeElement.children[i].children[0], 'background', "radial-gradient(transparent 35%, rgb(32, 82, 171) 36%)");
        } else {
          this
            .renderer
            .setElementStyle(this.listRef.nativeElement.children[i].children[0], 'background', "transparent");
        }
        this
          .renderer
          .setElementStyle(this.listRef.nativeElement.children[i].children[0], 'transform', "skew(" + ((90 - centralAngel) * -1) + "deg) rotate(" + ((centralAngel / 2) - 90) + "deg)");
        this
          .renderer
          .setElementStyle(this.listRef.nativeElement.children[i].children[0].children[0], 'transform', "rotate(" + contentTransformation + "deg)");
        contentTransformation = contentTransformation - contentTransformAmount;
      }
    }
    // button


    this
      .renderer
      .setElementStyle(this.menuButtonRef.nativeElement, 'background-color', '#232323');
    let movarr = [4, 3, 2, 1, 0, 7, 6, 5]
    let start = movarr.shift();
    setTimeout(() => {
      this.move(start, movarr)
    }, 500);
  }

  setElementWithoutAnimation(elemIndex, degree) {
    // debugger
    this
      .renderer
      .setElementStyle(this.listRef.nativeElement.children[elemIndex], "transition", "none");

    this
      .renderer
      .setElementStyle(this.listRef.nativeElement.children[elemIndex], "transform", "rotate(" + degree + "deg) skew(" + (90 - this.centeralAngleL) + "deg)");
  }

  setElement(elementIndex, degree = 270, visible = 'unset', transition = "200ms ease-in-out") {
    this
      .renderer
      .setElementStyle(this.listRef.nativeElement.children[elementIndex], "transform", "rotate(" + degree + "deg) skew(" + (90 - this.centeralAngleL) + "deg)");
    if (visible == 'none') {
      setTimeout(() => {
        this
          .renderer
          .setElementStyle(this.listRef.nativeElement.children[elementIndex], "display", visible);
      }, 800);
    } else {
      this
        .renderer
        .setElementStyle(this.listRef.nativeElement.children[elementIndex], "display", visible);
    }
    this
      .renderer
      .setElementStyle(this.listRef.nativeElement.children[elementIndex], "transition", transition);
  }

  move(current, movementArr: number[]) {
    if (typeof (current) == 'undefined' && current == null) {
      return;
    }
    let transformationDegree = this.transformationArray[current];
    if (transformationDegree > 314) {
      transformationDegree = transformationDegree - 360;
    }
    for (let i = 0; i < movementArr.length; i++) {
      const element = movementArr[i];
      this.setElement(element, transformationDegree);
    }
    this.setElement(current, transformationDegree);
    let nextElem = movementArr.shift();
    setTimeout(() => {
      this.move(nextElem, movementArr);
    }, 220);
  }

  prevDegree = 0;


  moveBack(current, fullArr, currentArr) {
    if (typeof (current) == 'undefined' && current == null) {
     return;
    }
    currentArr.push(current);
    let transition = '200ms ease-in-out';
    if (currentArr.length == 1) {
      transition = 'none';
    }
    console.log(transition);
    
    let transformationDegree = this.transformationArray[current];

    for (let i = 0; i < currentArr.length; i++) {
      // const element = currentArr[i];
      this.setElement(current, transformationDegree, 'none', transition)
    }
    if (typeof (current) == 'undefined' && current == null) {
      return;
    }
    let nextElem = fullArr.shift();
    setTimeout(() => {
      this.moveBack(nextElem, fullArr, currentArr);
    }, 200);
  }

  getMyMovements(me) {
    let movarr = [4, 3, 2, 1, 0, 7, 6, 5]
    let index = movarr.indexOf(me);
    if (index == movarr.length - 1) {
      return movarr;
    }
    else if (index == 0) {
      movarr.push(movarr.shift())
      console.log(movarr);
      return movarr;
    } else {
      let arr1 = movarr.slice(0, index + 1);
      let arr2 = movarr.slice(index + 1, movarr.length);
      console.log(arr2.concat(arr1));
      return arr2.concat(arr1);
    }
  }

  onClick(event) {
    if (this.closeOnBlur) {
      // check if clicked outside this element
      if (!this.elemRef.nativeElement.contains(event.target)) {
        this.closeNav();
        console.log('clicked outside closing Nav...');
      }
    }
  }

  logSettings() {
    console.log('CircularTab Settings:');
    console.log('totalAngle: ' + this.totalAngle);
    console.log('gapAngle: ' + this.gapAngle);
    console.log('startAngle: ' + this.startAngle);
    console.log('closeOnTabSelection: ' + this.closeOnTabSelection);
    console.log('closeOnBlur: ' + this.closeOnBlur);
    console.log('closedBtnText: ' + this.closedBtnText);
    console.log('openedBtnText: ' + this.openedBtnText);
    console.log('closedBtnIconName: ' + this.closedBtnIconName);
    console.log('openedBtnIconName: ' + this.openedBtnIconName);
  }

}
