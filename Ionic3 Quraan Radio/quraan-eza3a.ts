import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AboutUsPage } from '../about-us/about-us';
import { Media, MediaObject } from '@ionic-native/media';
import { trigger, transition, useAnimation } from '@angular/animations';
import { bounceIn } from 'ng-animate';
import { MusicControls } from '@ionic-native/music-controls';
import { BackgroundMode } from "@ionic-native/background-mode";

@IonicPage()

@Component({
  selector: 'page-quraan-eza3a',
  templateUrl: 'quraan-eza3a.html',
  animations: [
    trigger('bounce', [transition('dd <=> cc', useAnimation(bounceIn))])
  ]
})

export class QuraanEza3aPage {


  bounce = {
    value: 'cc'
  };

  isPlaying = false;
  isInit = true;
  file: MediaObject = this.media.create('http://quraan.us:9944/');
// https://cmt-tech.cdn.warpcache.net/teb002/55D6D-radio6/index.m3u8
//https://www.youtube.com/watch?v=nCorHIE4t_w
  constructor(public navCtrl: NavController, public navParams: NavParams,
    private back: BackgroundMode,
    private musicControls: MusicControls,
    private media: Media
  ) {
    this.bounce = {
      value: 'cc'
    };
    this.back.on('activate').subscribe(() => {
      console.log('going to backgrgound mode ');
      this.musicControlsConfig();
    });

    this.back.on('deactivate').subscribe(() => {
      this.musicControls.destroy();
      console.log('comming from backgrgound mode ');
    });

  }

  musicControlsConfig() {
    this.musicControls.create({
      track: 'إذاعة القرآن الكريم',
      artist: 'الحرس الوطني',
      cover: 'assets/imgs/mainmark.png',
      isPlaying: true,
      dismissable: true,
      hasPrev: false,
      hasNext: false,
      hasClose: true,
      album: 'الحرس الوطني (إذاعة القرآن الكريم)',
      hasScrubbing: false,
      ticker: 'إذاعة القرآن الكريم',
      playIcon: 'media_play',
      pauseIcon: 'media_pause'
    });

    this.musicControls.subscribe().subscribe(action => {
      const message = JSON.parse(action).message;
      switch (message) {
        case 'music-controls-pause':
          this.isPlaying = false;
          this.file.pause();
          this.musicControls.updateIsPlaying(false);
          break;
        case 'music-controls-play':
          this.isPlaying = true;
          this.file.play();
          this.musicControls.updateIsPlaying(true);
          break;
        case 'music-controls-destroy':
          this.file.stop()
          this.musicControls.destroy();
          break;
        case 'music-controls-toggle-play-pause': {
          if (this.isPlaying) {
            this.pause();
          } else {
            this.play();
          }
        }
          break;
        default:
          break;
      }
    });
    this.musicControls.listen(); // activates the observable above
  }

  play() {
    this.isPlaying = true;
    this.file.play();
    this.onDone(null);
  };

  pause() {
    this.isPlaying = false;
    this.onDone(null);
    this.file.pause();
  };

  Back() {
    this.navCtrl.pop()
  }

  onDone($event) {
    if (this.isPlaying) {
      let x = this.bounce.value == 'dd' ? 'cc' : 'dd';
      this.bounce = {
        value: x
      }
    } else {
      this.bounce = {
        value: 'ff'
      }
    }
  }

  gotaboutpage() {
    this.navCtrl.push(AboutUsPage);
  }
}